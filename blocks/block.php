<div class="default-block">
    <div class="row">
        <div class="col col-full">
            <? if ($block['subject'] != "") { ?>
                <header class="block-title"><h3><? echo $block['subject']; ?></h3></header>
            <? } ?>
            <? if ($block['body'] != "") { ?>
                <div class="block-content"><? echo $block['body']; ?></div>
            <? } ?>
        </div>
    </div>
</div>