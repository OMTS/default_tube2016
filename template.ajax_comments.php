<?php 
    include_once("../../mb.php");
    if(is_numeric($_GET['id'])) {
        $id = $_GET['id']; 
    }
    
    $type = (int)$_GET['type'];	
    $loaded = "";
    if($_GET['loaded']) {
        $loaded = " LIMIT 0, 1";
    }
	
    $cresult = dbQuery("SELECT *, (SELECT username FROM users WHERE users.record_num = comments.userid) AS username, (SELECT avatar FROM users WHERE users.record_num = comments.userid) AS avatar, (SELECT gender FROM users WHERE users.record_num = comments.userid) AS gender FROM comments WHERE content = '$id' AND type = '".$type."' ORDER BY timestamp DESC".$loaded,false);
    if(count($cresult) > 0) {
?>
	<ul class="ul-comments" id="ul-comments">
            <?php 
                foreach($cresult as $crow) { 			
                    if(isset($crow['username']) && $crow['username'] != ""){		
                        $username = $crow['username'];	
                        $link = generateUrl('user',$crow['username'],$crow['userid']);
                    } else {
                        $username = _t("Anonymous");
                        $link = '';
                    }	
            ?>
            <li>	
                <?php if($link) echo '<a href="'.$link.'">';  ?>                                
                    <?php if($crow['avatar'] != '' && file_exists("$basepath/media/misc/$crow[avatar]")){ ?>
                        <img src='<?php echo $basehttp; ?>/media/misc/<?php echo $crow[avatar]; ?>' alt= '<?php echo ucwords($crow['username']); ?>'>
                    <?php } else { ?>
                        <?php if(strtolower($crow['gender']) == 'male'){ ?>
                            <img src='<?php echo $basehttp; ?>/core/images/avatar_male.png'  alt= '<?php echo ucwords($crow['username']); ?>'>
                        <?php } elseif(strtolower($crow['gender']) == 'female'){ ?>
                            <img src='<?php echo $basehttp; ?>/core/images/avatar_female.png'  alt= '<?php echo ucwords($crow['username']); ?>'>
                        <?php } else { ?>
                            <img src='<?php echo $basehttp; ?>/core/images/avatar_default.png'  alt= '<?php echo ucwords($crow['username']); ?>'>
                        <?php } ?>
                    <?php } ?>                                                        
                <?php if($link)  echo '</a>';  ?>
					
                <div class="block">
                    <?php if($link) echo '<a href="'.$link.'">';  ?>
                    <strong><?php echo $username; ?></strong>
                    <?php if($link)  echo '</a>';  ?> 
                    <?php echo _t("wrote") ?> <?php echo datediff('n',$crow[timestamp],time(),true); ?> <?php echo _t("ago") ?>: 
                    <p><i><?php echo $crow[comment]; ?></i></p>
                </div>
            </li>
            <?php } ?>
	</ul>
<?php } else { ?>
    <?php if($type == 0){ ?>
<p><?php echo _t("There are no comments for this video. Please leave your feedback and be the first!") ?></p>
    <?php } elseif($type == 1){ ?>
    <p><?php echo _t("There are no comments for this model. Please leave your feedback and be the first!") ?></p>
    <?php } else { ?>
    <p><?php echo _t("There are no comments for this profile. Please leave your feedback and be the first!") ?></p>
    <?php } ?>
<?php } ?>