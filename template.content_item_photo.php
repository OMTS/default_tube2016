<?php
$link = generateUrl('galleries', $row['title'], $row['record_num']);
$dirname = str_replace('.flv', '', $row['orig_filename']);
$subdir = $row['filename'][0] . '/' . $row['filename'][1] . '/' . $row['filename'][2] . '/' . $row['filename'][3] . '/' . $row['filename'][4] . '/';
$dirname = $subdir . $dirname;
$counter = dbValue("SELECT COUNT(*) AS `count` FROM `images` WHERE `gallery` = '$row[record_num]'", 'count',true);
if (!$row['thumbfile']) {
    $row['thumbfile'] = dbValue("SELECT `filename` FROM `images` WHERE `record_num` = '$row[thumbnail]'", 'filename',true);
}
?>
<!-- item -->
<div class="item-col col">
    <div class="item-inner-col inner-col">
        <a href="<? echo $link; ?>" title="<? echo htmlentities($row['title'], ENT_QUOTES, 'UTF-8'); ?>">
            <span class="image">
                <img src="<? echo $gallery_url; ?>/<? echo $row['filename']; ?>/thumbs/<? echo $row['thumbfile']; ?>" alt="<? echo htmlentities($row['title'], ENT_QUOTES, 'UTF-8'); ?>">
                <span class="time"><?php echo _t("Photos") ?>: <? echo $counter; ?></span>
            </span>
            <span class="item-info">
                <span class="title"><? echo $row['title']; ?></span>
                <span class="item-stats">
                    <span class="s-elem s-e-rate">
                        <span class="icon i-thumbs-up"></span>
                        <span class="sub-desc"><? echo $row['rating']; ?>%</span>
                    </span>
                    <span class="s-elem s-e-views">
                        <span class="icon i-eye"></span>
                        <span class="sub-desc"><? echo $row['views']; ?></span>
                    </span>
					<? if($row['access_level'] > 0) { ?>
					<span class="s-elem s-e-views">
						<span class="icon i-lock"></span>
						<span class="sub-desc"><? if($row['access_level'] == 1) { echo _t("Private"); } else { echo _t("Premium"); } ?></span>
					</span>
					<? } ?>
                </span>
            </span>
        </a>
        <a href="<? if ($_GET['mode'] == 'favorites') { ?><? echo $basehttp; ?>/action.php?action=remove_favorites&id=<? echo $row['record_num']; ?><? } else { ?><? echo $basehttp; ?>/action.php?action=add_favorites&id=<? echo $row['record_num']; ?><? } ?>" data-mb="modal" data-opt-type="ajax" data-opt-close="<?php echo _t("Close") ?>" data-toggle="tooltip" title="<? if ($_GET['mode'] == 'favorites') { ?><?php echo _t("Remove from favorites") ?><? } else { ?><?php echo _t("Add to favorites") ?><? } ?>" class="add-to-fav"><? if ($_GET['mode'] == 'favorites') { ?><span class="icon i-minus-o"></span><? } else { ?><span class="icon i-plus-o"></span><? } ?></a>
        <? if ($_SESSION['userid'] && ($row['submitter'] == $_SESSION['userid'])) { ?><a href="<? echo $basehttp; ?>/edit-content/?id=<? echo $row['record_num']; ?>" title="<?php echo _t("Edit") ?>" class="edit-content"><span class="icon i-edit"></span></a><? } ?>
    </div>
</div>
<!-- item END -->