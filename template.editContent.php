<!-- textpage -->
<div class="textpage-col textpage--contactform col">
    <div class="textpage-inner-col inner-col">

        <div class="row form-row">

            <!-- form -->
            <div class="form-col form--contact col">
                <div class="form-inner-col inner-col">
                    <form class="form-block" id="edit-content-form"  name="formEditContent" method="post" action="">


                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col col-full">
                                <div class="form-item-inner-col inner-col">
                                    <label><?php echo _t("Title"); ?> (<?php echo _t("30-100 chars") ?>)</label>
                                    <input class="form-control" name="title" type="text" id="title" value="<?php echo $row[title]; ?>" maxlength="100" onKeyup="document.getElementById('charlentitle').innerHTML = 'Length: ' + document.getElementById('title').value.length;"  onChange="document.getElementById('charlentitle').innerHTML = 'Length: ' + document.getElementById('title').value.length;" /><span id='charlentitle'> <?php echo _t("Length"); ?>: <?php echo strlen($row[title]); ?></span>
                                </div>
                            </div>
                            <!-- form-item END -->

                        </div>
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col col-full">
                                <div class="form-item-inner-col inner-col">
                                    <label><?php echo _t("Description"); ?> (<?php echo _t("150-250 chars") ?>)</label>
                                    <textarea class="form-control" name="description" type="text" id="description" onKeyup="document.getElementById('charlendescription').innerHTML = 'Length: ' + document.getElementById('description').value.length;"  onChange="document.getElementById('description').innerHTML = 'Length: ' + document.getElementById('description').value.length;" /><?php echo $row[description]; ?></textarea><span id='charlendescription'> <?php echo _t("Length"); ?>: <?php echo strlen($row[description]); ?></span>
                                </div>
                            </div>
                            <!-- form-item END -->

                        </div>
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col col-full">
                                <div class="form-item-inner-col inner-col">
                                    <label><?php echo _t("Keywords"); ?></label>
                                    <input class="form-control" name="keywords" type="text" id="keywords" value="<?php echo $row[keywords]; ?>" maxlength="255"> (<b><?php echo _t("Comma"); ?></b> <?php echo _t("Separated"); ?>)
                                </div>
                            </div>
                            <!-- form-item END -->

                        </div>
						
						<div class='row'>
							<div class="form-item-col col col-full">
                                            <div class="form-item-inner-col inner-col">
                                                <label><?php echo _t("Access Level") ?></label>
                                                <select name="access_level" id="select2" class="select-short" data-style="btn-selectpicker">
                                                    <option <?
                                                    if ($row['access_level'] == '0') {
                                                        echo 'selected';
                                                    }
                                                    ?> value='0'><?php echo _t("Public") ?></option>
                                                    <option <?
                                                    if ($row['access_level'] == '1') {
                                                        echo 'selected';
                                                    }
                                                    ?> value='1'><?php echo _t("Private") ?></option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- form-item END -->

						</div>
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col col-full">
                                <div class="form-item-inner-col inner-col">
                                    <label><?php echo _t("Select up to %max_categories Categories", array("%max_categories" => "<b>6</b>")); ?></label>
                                    <?php
                                    unset($thisniche);
                                    $sresult = dbQuery("SELECT * FROM content_niches WHERE content = '$id'", false);
                                    foreach ($sresult as $srow) {
                                        $thisniche[] = $srow[niche];
                                    }
                                    ?>
                                    <select name='niche[]' size="10" multiple="multiple">
                                        <?php
                                        $presult = dbQuery("SELECT * FROM niches ORDER BY name ASC", false);
                                        foreach ($presult as $srow) {
                                            if (in_array($srow[record_num], $thisniche)) {
                                                $checked = 'selected';
                                            } else {
                                                $checked = '';
                                            }
                                            echo "<option $checked value='$srow[record_num]'>$srow[name]</option>";
                                        }
                                        ?>
                                    </select>
                                    <?php echo _t("(Hold CTRL to make multiple selections)"); ?>
                                </div>
                            </div>
                            <!-- form-item END -->

                        </div>
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col form-item--actions">
                                <div class="form-item-inner-col inner-col">
                                    <button class="btn btn-default" type="submit" name="button"><span class="btn-label"><?php echo _t("Save"); ?></span></button>
                                </div>
                            </div>
                            <!-- form-item END -->
                        </div>
                    </form>
                </div>
            </div>
            <!-- form END -->
        </div>
    </div>
</div>
<!-- textpage END -->