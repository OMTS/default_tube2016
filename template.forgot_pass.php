<!-- textpage -->
<div class="textpage-col col">
    <div class="textpage-inner-col inner-col">
        
        <div class="row form-row">
            <!-- form -->
            <div class="form-col col">
                <div class="form-inner-col inner-col">
                    <form class="form-block" id="forgot-pass-form"  name="loginForm" method="post" action="">
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col col-full">
                                <div class="form-item-inner-col inner-col">
                                    <input class="form-control" id="signup_username" name="email" type="text" value="<? echo $thisusername; ?>" placeholder="<?php echo _t("Email") ?>">
                                </div>
                            </div>
                            <!-- form-item END -->
                        </div>
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col form-item--captcha">
                                <div class="form-item-inner-col inner-col">
                                    <div class="captcha-wrapper">
                                        <img src="<? echo $basehttp; ?>/captcha.php?<? echo time(); ?>" class="captcha captcha-img">
                                        <input class="form-control captcha-input" name="captchaaa" type="text" value="" placeholder="<?php echo _t("Human?") ?>">
                                    </div>
                                </div>
                            </div>
                            <!-- form-item END -->
                        </div>
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col form-item--actions">
                                <div class="form-item-inner-col inner-col">
                                    <button class="btn btn-default" type="submit" name="Submit"><span class="btn-label"><?php echo _t("Go!") ?></span></button>
                                </div>
                            </div>
                            <!-- form-item END -->
                        </div>
                    </form>
                </div>
            </div>
            <!-- form END -->
        </div>
    </div>
</div>
<!-- textpage END -->