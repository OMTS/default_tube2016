            <!-- VIDEO CONTENT -->
            <!-- stage -->
            <div class="stage-col col">
                <div class="stage-inner-col inner-col">
                    <div class="stage-gallery">
                        <div  class="inner-stage">
                            <div class="gallery-block">
                                <div class="inner-block responsive-thumbs">
                                    <?php
                                        //display single image if is selected
                                        if (is_numeric($_GET['image'])) {
                                            echo "<div data-mb='ajax-container' data-opt-close='"._t("Close")."' data-opt-title='"._t("Error")."' id='singleImage' data-opt-current='".$_GET['image']."'>";
                                            echo '<div class="loader"><div class="loader-inner ball-triangle-path"><div></div><div></div><div></div></div></div>';



                                            //generate previous and next button links
                                            $goToNext= "";
                                            $goToPrev= "";
                                            if ($_GET['image'] - 1 > 0) {
                                                $imagePrevLink = "$galleryUrl?image=" . ($_GET['image'] - 1);
                                                $goToPrev= $_GET['image'] - 1;
                                            } else {
                                                $imagePrevLink = $galleryUrl;
                                            }
                                            if ($_GET['image'] + 1 <= count($result)) {
                                                $imageNextLink = "$galleryUrl?image=" . ($_GET['image'] + 1);
                                                $goToNext= $_GET['image'] + 1;
                                            } else {
                                                $imageNextLink = $galleryUrl;
                                            }
                                            //show image and prev/next
                                    ?>
                                    <a href="<?php echo $imageUrl; ?>" data-lightbox="gallery" title="<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>"><img src="<?php echo $imageUrl; ?>" alt="<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?> (<?php echo $_GET['image']; ?>/<?php echo count($result) ?>)"></a>
                                    <?php if (count($result) > 1) { ?>
                                        <div class="gallery-controls">
                                            <?php if ($_GET['image'] > 1) { ?><a rel="prev" data-opt-next="<?php echo $goToPrev; ?>" data-mb="load-img" data-toggle="tooltip" class="gallery-nav gallery-prev" title="<?php echo _t("Previous") ?> (<?php echo $goToPrev ?>/<?php echo count($result) ?>)" href="<?php echo $imagePrevLink; ?>"><span class="icon i-prev"></span></a><?php } ?>
                                            <?php if ($_GET['image'] < count($result)) { ?><a rel="next" data-opt-next="<?php echo $goToNext; ?>" data-mb="load-img" data-toggle="tooltip" class="gallery-nav gallery-next" title="<?php echo _t("Next") ?> (<?php echo $goToNext ?>/<?php echo count($result) ?>)" href="<?php echo $imageNextLink; ?>"><span class="icon i-next"></span></a><?php } ?>
                                        </div>
                                    <?php

                                            }
                                            echo "</div>";
                                        }

                                        //display all images
                                        echo "<div id='galleryImages' class='row'>";
                                        if (is_numeric($_GET['image'])) {
                                            echo '<div class="h-slider" data-mb="slider" data-opt-current-img="' . $_GET['image'] . '" data-opt-img-max="' . count($result) . '">';
                                        }

                                        foreach ($result as $k => $row) {
                                            $j++;
                                    ?>
                                        <div class='gallery-item-col col gi-<?php echo $j; ?> <?php echo ($j == $_GET['image']) ? "active" : ""; ?>' >
                                            <div class="inner-block">
                                                <a <?php if(is_numeric($_GET['image'])) { ?>data-mb="load-img" data-opt-next="<?php echo $k + 1; ?>"<?php } ?> title="Image <?php echo $k + 1; ?>/<?php echo count($result) ?>" href="<?php echo $galleryUrl; ?>?image=<?php echo $k + 1; ?>"><img src="<?php echo $gallery_url; ?>/<?php echo $rrow[filename]; ?>/thumbs/<?php echo $row[filename]; ?>" alt="<?php echo $row[title]; ?>" ></a>
                                            </div>
                                        </div>




                                    <?php
                                        }

                                        if (is_numeric($_GET['image'])) {
                                            echo '</div>';
                                        }

                                        if (is_numeric($_GET['image'])) {
                                            echo '<div class="gallery-thumbs-option text-center"><a href="'.$galleryUrl.'" class="btn btn-default">'._t("Show all thumbs").'</a></div>';
                                        }

                                        if (false) {
                                    ?>
                                    <div class="gallery-item-col-paginator col">
                                        <div class="inner-block">
                                            <div class="row pagination">
                                                <div class="pagination-items col">
                                                    <div class="pagination-bg">
                                                        <div class="contents">
                                                            <?php foreach ($result as $k => $row) { ?>
                                                                <a <?php if ($_GET['image'] == $k + 1) { ?>class="jp-current"<?php } ?> href="<?php echo $galleryUrl; ?>?image=<?php echo $k + 1; ?>"><?php echo $k + 1; ?></a>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        }

                                        if (count($result) == 0) {
                                            echo "Sorry, this gallery contains no photos.";
                                        }
                                        echo "</div>";
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- stage END -->



            <!-- item-tr -->
            <div class="item-tr-col col">
                <div class="item-tr-inner-col inner-col">
                    <h1><?php echo $rrow['title']; ?></h1>


                    <!-- rating -->
                    <div class="rating-col col">
                        <div class="rating-inner-col inner-col">
                            <?php include($basepath . '/includes/rating/templateTh.php'); ?>
                        </div>
                    </div>
                    <!-- rating END -->
                </div>
            </div>
            <!-- item-tr END -->




            <!-- item-tabs -->
            <div class="item-tabs-col col">
                <div class="item-tabs-inner-col inner-col">
                    <ul class="tabs-list">
                        <li class="active"><a href="#" title="<?php echo _t("About") ?>" data-mb="tab" data-opt-tab="about"><span class="icon i-info"></span><span class="sub-label"><?php echo _t("About") ?></span></a></li>
                        <li><a data-mb="tab" href="#" title="<?php echo _t("Share") ?>" data-opt-tab="share"><span class="icon i-share"></span><span class="sub-label"><?php echo _t("Share") ?></span></a></li>

                        <li><a href="<?php echo $basehttp; ?>/action.php?action=add_favorites&id=<?php echo $rrow['record_num']; ?>" data-mb="modal" data-opt-type="ajax" data-opt-close="<?php echo _t("Close") ?>" data-toggle="tooltip" title="<?php echo _t("Add to favorites") ?>"><span class="icon i-heart"></span></a></li>
                        <li><a href="<?php echo $basehttp; ?>/action.php?action=reportVideo&id=<?php echo $rrow['record_num']; ?>" data-mb="modal" data-opt-type="iframe" data-opt-iframe-width="100%"  data-opt-iframe-height="400px" data-toggle="tooltip" title="<?php echo _t("Report content") ?>"><span class="icon i-flag"></span></a></li>
                    </ul>

                </div>
            </div>
            <!-- item-tabs END -->



            <!-- tabs -->
            <div class="tabs-col col">
                <div class="tabs-inner-col inner-col">

                    <!-- TAB -->
                    <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="about">
                        <div class="row">
                            <!-- tab-block -->
                            <div class="tab-block-col col">
                                <div class="tab-block-inner-col inner-col">

                                    <div class="d-container">
                                        <div class="submitter-container">
                                            <span class="inner-wrapper">
                                                <?php if (!$rrow['submitter'] == 0) { ?><a href="<?php echo generateUrl('user', $rrow['username'], $rrow['submitter']); ?>" title="<?php echo $rrow['username']; ?>"><?php } ?>
                                                    <span class="avatar">
                                                        <span class="avatar-img">
                                                            <?php echo getUserAvatar($rrow['submitter']); ?>
                                                        </span>
                                                    </span>
                                                    <span class="user-name">
                                                        <span class="sub-label"><?php echo _t("Submitted by") ?></span>
                                                        <?php if ($rrow['submitter'] == 0) { ?><?php echo _t("Anonymous") ?><?php } else { ?><?php echo $rrow['username']; ?><?php } ?>
                                                    </span>
                                                    <?php if (!$rrow['submitter'] == 0) { ?></a><?php } ?>
                                            </span>
                                        </div>

                                        <div class="stats-container">
                                            <ul class="stats-list">
                                                <li><span class="icon i-photo"></span><span class="sub-label"><?php echo count($result); ?></span></li>
                                                <li><span class="icon i-eye"></span><span class="sub-label"><?php echo $rrow['views']; ?></span></li>
                                                <li><span class="icon i-calendar"></span><span class="sub-label"><?php echo $rrow['date_added']; ?></span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                    <? $_keywords = buildTags($rrow['keywords']); ?>
                                    <? $_channels = buildChannels($rrow['record_num']); ?>
                                    <? $_description = $rrow['description']; ?>
                                    <? $_pornstar = showPornstar($rrow['record_num'], "<li>", "</li>"); ?>
                                    
                                    <? if (!empty($_keywords) || !empty($_channels) || !empty($_description) || $_pornstar != false) { ?>
                                        <div class="expand-block-more" data-opt-expand-target="more-info">
                                            <? if (!empty($_description)) { ?>
                                                <div class="d-container">
                                                    <div class="main-description">
                                                        <? echo $_description; ?>
                                                    </div>
                                                </div>
                                            <? } ?>
                                            <? if ($_pornstar != false) { ?>
                                                <div class="d-container">
                                                    <div class="tags-block">
                                                        <span class="icon i-star"></span>
                                                        <span class="sub-label"><?php echo _t("Models") ?>:</span>
                                                        <ul class="models-list"><? echo $_pornstar; ?></ul>
                                                    </div>
                                                </div>
                                            <? } ?>
                                            <? if (!empty($_keywords)) { ?>
                                                <div class="d-container">
                                                    <div class="tags-block">
                                                        <span class="icon i-tag"></span>
                                                        <span class="sub-label"><?php echo _t("Tags") ?>:</span>
                                                        <? echo $_keywords; ?>
                                                    </div>
                                                </div>
                                            <? } ?>
                                            <? if (!empty($_channels)) { ?>
                                                <div class="d-container">
                                                    <div class="tags-block">
                                                        <span class="icon i-folder"></span>
                                                        <span class="sub-label"><?php echo _t("Categories") ?>:</span>
                                                        <? echo $_channels; ?>
                                                    </div>
                                                </div>
                                            <? } ?>
                                        </div>
                                    <? } ?>
                                    
                                    <? if (!empty($_keywords) || !empty($_channels) || !empty($_description)) { ?>
                                        <!-- expand-trigger -->
                                        <div class="expand-trigger-col col">
                                            <div class="expand-trigger-inner-col inner-col">
                                                <a href="#" title="" class="btn btn-header" data-mb="expand" data-opt-target="more-info"><span class="sub-label-off"><?php echo _t("Show more") ?></span><span class="sub-label-on"><?php echo _t("Show less") ?></span></a>
                                            </div>
                                        </div>
                                        <!-- expand-trigger END -->
                                    <? } ?>

                                </div>
                            </div>
                            <!-- tab-block END -->
                        </div>
                    </div>
                    <!-- TAB END -->
                    <!-- TAB -->
                    <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="share">
                        <div class="row">
                            <!-- tab-block -->
                            <div class="tab-block-col col">
                                <div class="tab-block-inner-col inner-col">

                                    <div class="d-container">
                                        <div class="social-media-share">
                                            <ul class="share-list">
                                                <li><a data-mb="popup" data-opt-width="500" data-opt-height="400" href="http://www.facebook.com/share.php?u=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>&title=<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" class="social social-fb"><span class="icon i-fb"></span></a></li>
                                                <li><a data-mb="popup" data-opt-width="500" data-opt-height="400" href="http://twitter.com/home?status=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>+<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" class="social social-tw"><span class="icon i-tw"></span></a></li>
                                                <li><a data-mb="popup" data-opt-width="500" data-opt-height="400" href="http://www.tumblr.com/share?v=3&u=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>&t=<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>"><span class="icon i-tr"></span></a></li>
                                                <li><a data-mb="popup" data-opt-width="500" data-opt-height="400" href="https://plus.google.com/share?url=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" class="social social-gp"><span class="icon i-gp"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- tab-block END -->
                        </div>
                    </div>
                    <!-- TAB END -->


                </div>
            </div>
            <!-- tabs END -->
        </div>
    </div>
</div>

<?php if($allowDisplayComments) { ?>
    <div class="box-container">
        <div class="inner-box-container">
            <header class="row">
                <div class="title-col title-col--normal col">
                    <div class="title-inner-col inner-col">
                        <h1><?php echo _t("Comments") ?> <span class="dimmed-desc">(<?php echo getTotalComments($rrow['record_num']); ?>)</span></h1>
                    </div>
                </div>
            </header>
            <div class="row">
                <!-- comments -->
                <section class="comments-col col" id="comments">
                    <div class="comments-inner-col inner-col">
                        <?
                        $contentID = $rrow['record_num'];
                        $commentsType = 0;
                        include('widgets/widget.comments.php');
                        ?>
                    </div>
                </section>
                <!-- comments END -->
            </div>
        </div>
    </div>
<?php } ?>