<!-- textpage -->
<div class="textpage-col col">
    <div class="textpage-inner-col inner-col">
        <div class="row form-row">
            <!-- form -->
            <div class="form-col col">
                <div class="form-inner-col inner-col">
                    <form class="form-block" id="login-form"  name="loginForm" method="post" action="">
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col col-half">
                                <div class="form-item-inner-col inner-col">
                                    <input class="form-control" id="ahd_username" name="ahd_username" autocomplete='off' type="text" value="" placeholder="<?php echo _t("Username") ?>">
                                </div>
                            </div>
                            <!-- form-item END -->
                            <!-- form-item -->
                            <div class="form-item-col col col-half">
                                <div class="form-item-inner-col inner-col">
                                    <input class="form-control" id="ahd_password" name="ahd_password" autocomplete='off' type="password" value="" placeholder="<?php echo _t("Password") ?>">
                                </div>
                            </div>
                            <!-- form-item END -->
                        </div>
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col form-item--links">
                                <div class="form-item-inner-col inner-col">
                                    <a href="<? echo $basehttp; ?>/forgot-pass"><?php echo _t("Forgot Password?") ?></a><br>
                                    <a href="<? echo $basehttp; ?>/signup"><?php echo _t("Not a member? Click here to sign up, its free!") ?></a>
                                </div>
                            </div>
                            <!-- form-item END -->
                        </div>
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col form-item--actions">
                                <div class="form-item-inner-col inner-col">
                                    <button class="btn btn-default" type="submit" name="Submit"><span class="btn-label"><?php echo _t("Login") ?></span></button>
                                </div>
                            </div>
                            <!-- form-item END -->
                        </div>

                        <? if (!$_SESSION['userid'] && ($enable_facebook_login || $enable_twitter_login)) { ?>
                            <div class="row social-logins">
                                <? if (!$_SESSION['userid'] && $enable_facebook_login) { ?>
                                    <!-- form-item -->
                                    <div class="form-item-col col form-item--social form-item--social-fb">
                                        <div class="form-item-inner-col inner-col">
                                            <?php include($basepath . '/facebook_login.php'); ?>					
                                            <a class="btn btn-social-login btn-social-fb" href="<? echo $basehttp; ?>/includes/facebook/facebook.php"><img src="<?php echo $basehttp; ?>/core/images/facebook-login-button.png" alt="<?php echo _t("Login by FaceBook") ?>" border="0" /></a>
                                        </div>
                                    </div>
                                    <!-- form-item END -->
                                <? } ?>
                                <? if (!$_SESSION['userid'] && $enable_twitter_login) { ?>
                                    <!-- form-item -->
                                    <div class="form-item-col col form-item--social form-item--social-tw">
                                        <div class="form-item-inner-col inner-col">
                                            <a href="<?php echo $twitter->getAuthenticateUrl(); ?>&force_login=true"><img src="<?php echo $basehttp; ?>/core/images/twitter-login-button.png" alt="<?php echo _t("Login by Twitter") ?>" /></a>
                                        </div>
                                    </div>
                                    <!-- form-item END -->
                                <? } ?>
                            </div>
                        <? } ?>

                    </form>
                </div>
            </div>
            <!-- form END -->
        </div>
    </div>
</div>
<!-- textpage END -->