<? $colorClass = ($_COOKIE["mbColorScheme"] == 1) ? " inverted-colors" : ""; ?><!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7<? echo $colorClass ?>"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie-7-only<? echo $colorClass ?>"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie-8-only<? echo $colorClass ?>"><![endif]-->
<!--[if gte IE 9]><!--> <html class="no-js no-filter<? echo $colorClass ?>"><!--<![endif]-->
    <head>
        <? getWidget('widget.header_scripts.php'); ?>
    </head>
    <body class="page-<? echo bodyClasses(); ?>">
        
        <section class="page-wrap">
            
            <div class="wrapper maintenance-mode" style="width:600px;max-width:90%;font-size:1rem;line-height:2em">
                
                <div class="text-center">
                    <img src="<? echo $template_url; ?>/images/logo.png" alt="<?php echo _t("Home") ?> - <? echo $sitename; ?>" style="margin-bottom:50px">
                    <? echo getMessages(); ?>
                </div>
                
            </div>
            
        </section>
        
        <!--[if IE]><script src="<? echo $template_url; ?>/js/ie/ie10fix.js" title="viewport fix"></script><![endif]-->
        <!--[if lt IE 9]><script src="<? echo $template_url; ?>/js/ie/ie.min.js"></script><script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script><![endif]-->
        
    </body>
</html>