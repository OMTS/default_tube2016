<?
$link = generateUrl('user', $row['username'], $row['record_num']);
?>
<!-- item -->
<div class="item-col item--member col">
    <div class="item-inner-col inner-col">
        <a href="<? echo $link; ?>" title="<? echo ucwords($row['username']); ?>">
            <span class="image">
                <? if($row['avatar'] != '' && file_exists("$basepath/media/misc/$row[avatar]")){ ?>
                    <img src='<? echo $basehttp; ?>/media/misc/<? echo $row[avatar]; ?>' alt= '<? echo ucwords($row['username']); ?>'>
                <? } else { ?>
                    <? if(strtolower($row['gender']) == 'male'){ ?>
                        <img src='<? echo $basehttp; ?>/core/images/avatar_male.png'  alt= '<? echo ucwords($row['username']); ?>'>
                    <? } elseif(strtolower($row['gender']) == 'female'){ ?>
                        <img src='<? echo $basehttp; ?>/core/images/avatar_female.png'  alt= '<? echo ucwords($row['username']); ?>'>
                    <? } else { ?>
                        <img src='<? echo $basehttp; ?>/core/images/avatar_default.png'  alt= '<? echo ucwords($row['username']); ?>'>
                    <? } ?>
                <? } ?>
            </span>
            <span class="item-info">
                <span class="title">
                    <? echo ucwords($row['username']); ?>
                </span>
                <span class="item-stats">
                    <span class="s-elem s-e-age">
                        <span class="sub-label"><?php echo _t("Age") ?>:</span>
                        <span class="sub-desc"><? echo ($row['age']) ? $row['age'] : "N/A"; ?></span>
                    </span>
                    <span class="s-elem s-e-gender">
                        <span class="sub-label"><?php echo _t("Gender") ?>:</span>
                        <span class="sub-desc"><? echo ($row['gender']) ? _t($row['gender']) : "N/A"; ?></span>
                    </span>
                </span>

            </span>
        </a>
    </div>
</div>
<!-- item END -->