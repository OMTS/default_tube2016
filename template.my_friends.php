<?php
    $resultNew = dbQuery("SELECT  friends.record_num as friend_id, friends.friend_request_message, friends.date_added,  users.record_num, users.username FROM friends, users 
		          WHERE friends.friend = '$_SESSION[userid]' AND users.record_num =  friends.user AND approved = 0 ORDER BY friends.date_added", false);
?>
<div class="profilepage-col col">
    <div class="profilepage-inner-col offset-columns inner-col">
        <!-- aside-tabs -->
        <aside class="aside-tabs-col">
            <div class="aside-tabs-inner-col inner-col">
                <div class="profile-img-avatar">
                    <?php if ($urow['avatar'] != '' && file_exists("$basepath/media/misc/$urow[avatar]")) { ?>
                        <img src='<?php echo $basehttp; ?>/media/misc/<?php echo $urow[avatar]; ?>' alt= '<?php echo ucwords($urow['username']); ?>'>
                    <?php } else { ?>
                        <?php if (strtolower($urow['gender']) == 'male') { ?>
                            <img src='<?php echo $basehttp; ?>/core/images/avatar_male.png'  alt= '<?php echo ucwords($urow['username']); ?>'>
                        <?php } elseif (strtolower($urow['gender']) == 'female') { ?>
                            <img src='<?php echo $basehttp; ?>/core/images/avatar_female.png'  alt= '<?php echo ucwords($urow['username']); ?>'>
                        <?php } else { ?>
                            <img src='<?php echo $basehttp; ?>/core/images/avatar_default.png'  alt= '<?php echo ucwords($urow['username']); ?>'>
                        <?php } ?>
                    <?php } ?>
                </div>
                <?php if ($_SESSION['userid'] && ($urow[record_num] !== $_SESSION['userid'])) { ?>
                    <a title="<?php echo _t("Add To Friends") ?>" data-mb="modal" data-opt-close="<?php echo _t("Close") ?>" data-opt-type="iframe" data-opt-iframe-width="100%"  data-opt-iframe-height="80px" href="<?php echo $basehttp; ?>/includes/inc.add_friend.php?option=add&id=<?php echo $urow['record_num']; ?>" id="addAsFriend3" class="btn btn-full btn-default btn-add-to-friends"><?php echo _t("Add To Friends") ?></a>
                <?php } ?>
                <ul class="tabs-list tabs-list--aside">
                    <li class="active"><a data-mb="tab" data-opt-tab="new-invitations" title="<?php echo _t("New invitations") ?>" href="#"><span class="icon i-email"></span><span class="sub-label"><?php echo _t("New invitations") ?> (<?php echo count($resultNew); ?>)</span></a></li>
                    <li><a data-mb="tab" data-opt-tab="my-friends" title="<?php echo _t("My Friends") ?>" href="#"><span class="icon i-group"></span><span class="sub-label"><?php echo _t("My Friends") ?></span></a></li>
                    <li><a data-mb="tab" data-opt-tab="sent-invitations" title="<?php echo _t("Sent Invitations") ?>" href="#"><span class="icon i-sent"></span><span class="sub-label"><?php echo _t("Sent Invitations") ?></span></a></li>
                </ul>
            </div>
        </aside>
        <!-- aside-tabs END -->



        <!-- profile-content -->
        <section class="profile-content-col">
            <div class="profile-content-inner-col inner-col">
                <!-- TABS GOES HERE -->
                <!-- TAB -->
                <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="new-invitations">
                    <div class="row">
                        <!-- tab-block -->
                        <div class="tab-block-col col">
                            <div class="tab-block-inner-col inner-col">
                                <div class="recent-uploads">
                                    <h2><?php echo _t("New invitations") ?> <span class="dimmed-desc">(<?php echo count($resultNew); ?>)</span></h2>
                                    <div class="table-responsive">
                                        <form method="post" action="">
                                            <table width="100%" border="0" class="sTable">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="150"><div align="left"><?php echo _t("User") ?></div></th>
                                                        <th class="text-center"><div align="left"><?php echo _t("Message") ?></div></th>
                                                        <th class="text-center"><div class="text-center"><?php echo _t("Approve") ?></div></th>	
                                                        <th class="text-center"  width="90"><div align="left"><?php echo _t("Sent") ?></div></th>
                                                        <th class="text-center"  width="60"><div class="text-center"><?php echo _t("Delete") ?></div></th>
                                                    </tr>	
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if ($resultNew) {
                                                            foreach ($resultNew as $nrow) {
                                                                $link = generateUrl('user', $nrow[username], $nrow[record_num]);
                                                    ?>
                                                    <tr id="friend<?php echo $nrow[friend_id]; ?>">
                                                        <td><a href="<?php echo $link; ?>"><?php echo $nrow[username]; ?></a></td>
                                                        <td><?php echo $nrow[friend_request_message]; ?></td>
                                                        <td>
                                                            <div class="text-center">
                                                                <a onClick="javascript:approveFriend('<?php echo $nrow[friend_id]; ?>'); return false;" href="#" class="addIco">
                                                                    <span class="icon i-ok"></span>
                                                                </a>
                                                            </div>
                                                        </td>
                                                        <td><strong><?php echo datediff('', $nrow[date_added], date('Y-m-d H:i:s'), false); ?></strong> ago</td>        
                                                        <td class="text-center">
                                                            <div class="text-center">
                                                                <a onClick="javascript:deleteFriend('<?php echo $nrow[friend_id]; ?>'); return false;" href="#" class="removeIco">
                                                                    <span class="icon i-delete"></span>
                                                                </a>
                                                            </div>
                                                        </td>		
                                                    </tr>
                                                    <?php } ?>				
                                                    <td colspan="5" class="no-border">
                                                        <a class="btn btn-default btn-sm" href="<?php echo $basehttp; ?>/my-friends?acceptAll=1"><?php echo _t("Accept ALL") ?></a>  <a href="<?php echo $basehttp; ?>/my-friends?declineAll=1" class="btn btn-default btn-sm"><?php echo _t("Decline  ALL") ?></a>
                                                    </td>	
                                                <?php } else {
                                                    ?>
                                                    <td colspan="5" class="no-border"><?php echo _t("No new invation") ?></td>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab-block END -->
                    </div>
                </div>
                <!-- TAB END -->
                <!-- TAB -->
                <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="my-friends">
                    <div class="row">
                        <!-- tab-block -->
                        <div class="tab-block-col col">
                            <div class="tab-block-inner-col inner-col">
                                <div class="user-friends">
                                    <h2><?php echo _t("My Friends") ?></h2>
                                    <div class="table-responsive">
                                        <form method="post" action="">
                                            <table width="100%" border="0" class="sTable">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="150"><div align="left"><a href="?sortby=name"><?php echo _t("User") ?></a></div></th>
                                                        <th class="text-center"><div align="left"><a href="?sortby=date"><?php echo _t("Date added") ?></a></div></th>			
                                                        <th class="text-center" width="60"><div class="text-center"><?php echo _t("Delete") ?></div></th>
                                                    </tr>	
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if (isset($_GET[sortby]) && $_GET[sortby] == 'name') {
                                                            $orderBy = ' users.username ASC ';
                                                        } else if (isset($_GET[sortby]) && $_GET[sortby] == 'date') {
                                                            $orderBy = ' friends.date_added DESC ';
                                                        } else {
                                                            $orderBy = 'friends.date_added DESC';
                                                        }
                                                        $resultFriends = dbQuery("SELECT  friends.record_num as friend_id, friends.friend_request_message, friends.date_added,  users.record_num, users.username FROM friends, users WHERE  friends.friend = '$_SESSION[userid]' AND users.record_num =  friends.user AND approved = 1 ORDER BY $orderBy", false);

                                                        if ($resultFriends) {
                                                            foreach ($resultFriends as $frow) {
                                                                $link = generateUrl('user', $frow[username], $frow[record_num]);
                                                    ?>
                                                    <tr id="friend<?php echo $frow[friend_id]; ?>">
                                                        <td><a href="<?php echo $link; ?>"><?php echo $frow[username]; ?></a></td>			
                                                        <td><strong><?php echo datediff('', $frow[date_added], date('Y-m-d H:i:s'), false); ?></strong> <?php echo _t("ago") ?></td>
                                                        <td class="text-center"><div class="text-center"><a onClick="javascript:deleteFriendApproved('<?php echo $frow[friend_id]; ?>');
                                                                        return false;" class="removeIco" href="#"><span class="icon i-delete"></span></a></div></td>		
                                                    </tr>
                                                    <?php
                                                            }
                                                        } else {
                                                    ?>
                                                    <td colspan="3"><?php echo _t("You have no friends") ?>.</td>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- tab-block END -->
                    </div>
                </div>
                <!-- TAB END -->
                <!-- TAB -->
                <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="sent-invitations">
                    <div class="row">
                        <!-- tab-block -->
                        <div class="tab-block-col col">
                            <div class="tab-block-inner-col inner-col">
                                <div class="user-wall">
                                    <h2><?php echo _t("Sent Invitations") ?></h2>
                                    <div class="table-responsive">
                                        <form method="post" action="">
                                            <table width="100%" border="0" class="sTable">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="150"><div align="left"><?php echo _t("User") ?></div></th>	
                                                        <th class="text-center"><div align="left"><?php echo _t("Message") ?></div></th>		
                                                        <th class="text-center" width="90"><div align="left"><?php echo _t("Sent") ?></div></th>			
                                                        <th class="text-center" width="60"><div class="text-center"><?php echo _t("Delete") ?></div></th>
                                                    </tr>	
                                                </thead>
                                                <tbody>
                                                <?php
                                                    $resultSent = dbQuery("SELECT  friends.record_num as friend_id, friends.friend_request_message, friends.date_added, users.record_num, users.username FROM friends, users WHERE  friends.user = '$_SESSION[userid]' AND users.record_num =  friends.friend AND  friends.approved = 0  ORDER BY friends.date_added DESC", false);
                                                    if ($resultSent) {
                                                        foreach ($resultSent as $srow) {
                                                            $link = generateUrl('user', $srow[username], $srow[record_num]);
                                                ?>
                                                    <tr id="friend<?php echo $srow[friend_id]; ?>">
                                                        <td><a href="<?php echo $link; ?>"><?php echo $srow[username]; ?></a></td>
                                                        <td><?php echo $srow[friend_request_message]; ?></td>
                                                        <td class="text-center"><div align="left"><strong><?php echo datediff('', $srow[date_added], date('Y-m-d H:i:s'), false); ?></strong> ago</div></td>	
                                                        <td class="text-center"><div class="text-center">
                                                                <a onclick="javascript:deleteFriend('<?php echo $srow[friend_id]; ?>');
                                                                                return false;" href="#" class="removeIco"><span class="icon i-delete"></span></a></div></td>		
                                                    </tr>
                                                <?php
                                                        }
                                                    } else {
                                                ?>
                                                    <tr><td colspan="4"><?php echo _t("No result") ?></td></tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- tab-block END -->
                    </div>
                </div>
                <!-- TAB END -->

                <!-- TABS GOES HERE END -->
            </div>
        </section>
        <!-- profile-content END -->


    </div>
</div>
<!-- profilepage END -->
<script>
    function deleteFriend(id) {
        $("#friend" + id).fadeOut("slow");
        $.get('<?php echo $basehttp; ?>/includes/ajax.delete_friend.php?id=' + id);
    }
    function deleteFriendApproved(id) {
        $("#friend" + id).fadeOut("slow");
        $.get('<?php echo $basehttp; ?>/includes/ajax.delete_friend.php?approved=1&id=' + id);
    }

    function approveFriend(id) {
        $("#friend" + id).fadeOut("slow");
        $.get('<?php echo $basehttp; ?>/includes/ajax.approve_friend.php?id=' + id);
    }

    $(document).ready(function () {
        $('#profile h4').click(function () {
            $(this).parent().children('.hidden').slideToggle('fast');
            if ($(this).children('.more').hasClass('off')) {
                $(this).children('.more').removeClass('off').addClass('on');
            } else {
                $(this).children('.more').removeClass('on').addClass('off');
            }
        });
    });
</script>