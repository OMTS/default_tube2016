<!-- profilepage -->
<div class="profilepage-col col">
    <div class="profilepage-inner-col offset-columns inner-col">
        
        <!-- aside-tabs -->
        <aside class="aside-tabs-col">
            <div class="aside-tabs-inner-col inner-col">
                <div class="profile-img-avatar">
                    <? if($urow['avatar'] != '' && file_exists("$basepath/media/misc/$urow[avatar]")){ ?>
                        <img src='<? echo $basehttp; ?>/media/misc/<? echo $urow[avatar]; ?>' alt= '<? echo ucwords($urow['username']); ?>'>
                    <? } else { ?>
                        <? if(strtolower($urow['gender']) == 'male'){ ?>
                            <img src='<? echo $basehttp; ?>/core/images/avatar_male.png'  alt= '<? echo ucwords($urow['username']); ?>'>
                        <? } elseif(strtolower($urow['gender']) == 'female'){ ?>
                            <img src='<? echo $basehttp; ?>/core/images/avatar_female.png'  alt= '<? echo ucwords($urow['username']); ?>'>
                        <? } else { ?>
                            <img src='<? echo $basehttp; ?>/core/images/avatar_default.png'  alt= '<? echo ucwords($urow['username']); ?>'>
                        <? } ?>
                    <? } ?>
                </div>
                <? if ($_SESSION['userid'] && ($urow[record_num] !== $_SESSION['userid'])) { ?>
                    <a title="<?php echo _t("Add To Friends") ?>" data-mb="modal" data-opt-close="<?php echo _t("Close") ?>" data-opt-type="iframe" data-opt-iframe-width="100%"  data-opt-iframe-height="80px" href="<? echo $basehttp; ?>/includes/inc.add_friend.php?option=add&id=<? echo $urow['record_num']; ?>" id="addAsFriend3" class="btn btn-full btn-default btn-add-to-friends"><?php echo _t("Add To Friends") ?></a>
                <? } ?>
                <ul class="tabs-list tabs-list--aside">
                    <li class="active"><a data-mb="tab" data-opt-tab="information" title="<?php echo _t("Information") ?>" href="#"><span class="icon i-info"></span><span class="sub-label"><?php echo _t("Information") ?></span></a></li>
                    <li><a data-mb="tab" data-opt-tab="uploads_v" title="<?php echo _t("Videos") ?>" href="#"><span class="icon i-video"></span><span class="sub-label"><?php echo _t("Videos") ?></span></a></li>
                    <li><a data-mb="tab" data-opt-tab="uploads_p" title="<?php echo _t("Photos") ?>" href="#"><span class="icon i-photo"></span><span class="sub-label"><?php echo _t("Photos") ?></span></a></li>
                    <li><a data-mb="tab" data-opt-tab="friends" title="<?php echo _t("Friends") ?><" href="#"><span class="icon i-group"></span><span class="sub-label"><?php echo _t("Friends") ?></span></a></li>
                    <li><a data-mb="tab" data-opt-tab="wall" title="<?php echo _t("Wall") ?><" href="#"><span class="icon i-wall"></span><span class="sub-label"><?php echo _t("Wall") ?></span></a></li>
                </ul>
            </div>
        </aside>
        <!-- aside-tabs END -->

        <!-- profile-content -->
        <section class="profile-content-col">
            <div class="profile-content-inner-col inner-col">
                <!-- TABS GOES HERE -->
                <!-- TAB -->
                <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="information">
                    <div class="row">
                        <!-- tab-block -->
                        <div class="tab-block-col col">
                            <div class="tab-block-inner-col inner-col">
                                <div class="profile-info">
                                    <div class="d-container">
                                        <h2><?php echo _t("User Information") ?></h2>

                                        <ul class="profile-list">
                                            <li class="profile-field profile-field-username"><span class="sub-label"><?php echo _t("Username") ?>:</span> <span class="sub-desc"><? echo $urow['username']; ?></span></li>
                                            <li class="profile-field profile-field-joined"><span class="sub-label"><?php echo _t("Joined") ?>:</span> <span class="sub-desc"><? echo date('F jS Y', strtotime($urow['date_joined'])); ?></span></li>
                                            <li class="profile-field profile-field-lastlogin"><span class="sub-label"><?php echo _t("Last Login") ?>:</span> <span class="sub-desc"><?
                                                    if ($urow['lastlogin']) {
                                                        echo date('Y-m-d \a\t H:i:s', $urow['lastlogin']);
                                                    } else {
                                                        echo 'Never';
                                                    }
                                                    ?></span></li>
                                            <? if ($urow['gender']) { ?>
                                                <li class="profile-field profile-field-gender"><span class="sub-label"><?php echo _t("Gender") ?>:</span> <span class="sub-desc"><? echo _t($urow['gender']); ?></span></li>
                                            <? } ?>
                                            <? if ($urow['age']) { ?>
                                                <li class="profile-field profile-field-age"><span class="sub-label"><?php echo _t("Age") ?>:</span> <span class="sub-desc"><? echo $urow['age']; ?></span></li>
                                            <? } ?>
                                            <? if ($urow['location']) { ?>
                                                <li class="profile-field profile-field-location"><span class="sub-label"><?php echo _t("Location") ?>:</span> <span class="sub-desc"><? echo $urow['location']; ?></span></li>
                                            <? } ?>
                                            <? foreach ($custom_user_fields as $k => $v) { ?>
                                                <? if ($custom[$k]) { ?>
                                                    <li class="profile-field profile-field-custom"><span class="sub-label"><? echo $k; ?>:</span> <span class="sub-desc"><? echo $custom[$k]; ?></span></li>
                                                <? } ?>
                                            <? } ?>  
                                            <? if ($urow['description']) { ?>
                                                <li class="profile-field profile-field-description"><span class="sub-label"><?php echo _t("A little about me...") ?></span> <span class="sub-desc"><? echo nl2br($urow['description']); ?></span></li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="profile-options">
                                    <div class="row">
                                        <!-- ucp-option -->
                                        <div class="ucp-option-col col">
                                            <div class="ucp-option-inner-col inner-col">
                                                <a href="<? echo $basehttp; ?>/edit-profile">
                                                    <span class="icon-holder">
                                                        <span class="icon i-edit"></span> 
                                                    </span>
                                                    <span class="sub-label">
                                                        <?php echo _t("Edit profile") ?>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- ucp-option END -->
                                        <!-- ucp-option -->
                                        <div class="ucp-option-col col">
                                            <div class="ucp-option-inner-col inner-col">
                                                <a href="<? echo $basehttp; ?>/mailbox/">
                                                    <span class="icon-holder">
                                                        <span class="icon i-email"></span> 
                                                    </span>
                                                    <span class="sub-label">
                                                        <?php echo _t("Messages") ?>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- ucp-option END -->
                                        <!-- ucp-option -->
                                        <div class="ucp-option-col col">
                                            <div class="ucp-option-inner-col inner-col">
                                                <a href="<? echo $basehttp; ?>/favorites/">
                                                    <span class="icon-holder">
                                                        <span class="icon i-heart"></span> 
                                                    </span>
                                                    <span class="sub-label">
                                                        <?php echo _t("Favorites") ?>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- ucp-option END -->
                                        <!-- ucp-option -->
                                        <div class="ucp-option-col col">
                                            <div class="ucp-option-inner-col inner-col">
                                                <a href="<? echo $basehttp; ?>/my-friends">
                                                    <span class="icon-holder">
                                                        <span class="icon i-group"></span> 
                                                    </span>
                                                    <span class="sub-label">
                                                        <?php echo _t("Friends") ?>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- ucp-option END -->
                                        <!-- ucp-option -->
                                        <div class="ucp-option-col ucp-option--sign-out col">
                                            <div class="ucp-option-inner-col inner-col">
                                                <a href="<? echo $basehttp; ?>/logout">
                                                    <span class="icon-holder">
                                                        <span class="icon i-sign-out"></span> 
                                                    </span>
                                                    <span class="sub-label">
                                                        <?php echo _t("Logout") ?>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                        <!-- ucp-option END -->
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <!-- tab-block END -->
                    </div>
                </div>
                <!-- TAB END -->
                <!-- TAB -->
                <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="uploads_v">
                    <div class="row">
                        <!-- tab-block -->
                        <div class="tab-block-col col">
                            <div class="tab-block-inner-col inner-col">
                                <div class="recent-uploads">
                                    <h2><? echo ucwords($urow['username']); ?>'s <?php echo _t("Recent Video Uploads") ?></h2>
                                    <div class="row">
                                        <? userRecentUploads($_SESSION[userid], 8); ?>
                                    </div>

                                    <div class="more more-tabs"><a class="btn btn-default btn-xs more-user-uploads-btn" href='<? echo $basehttp; ?>/uploads-by-user/<? echo $_SESSION[userid]; ?>/'><?php echo _t("View All") ?></a></div>
                                </div>
                            </div>
                        </div>
                        <!-- tab-block END -->
                    </div>
                </div>
                <!-- TAB END -->
                <!-- TAB -->
                <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="uploads_p">
                    <div class="row">
                        <!-- tab-block -->
                        <div class="tab-block-col col">
                            <div class="tab-block-inner-col inner-col">
                                <div class="recent-uploads">
                                    <h2><? echo ucwords($urow['username']); ?>'s <?php echo _t("Recent Photo Uploads") ?></h2>
                                    <div class="row">
                                        <? userRecentUploads($_SESSION[userid], 8, 'photos'); ?>
                                    </div>

                                    <div class="more more-tabs"><a class="btn btn-default btn-xs more-user-uploads-btn" href='<? echo $basehttp; ?>/uploads-by-user/<? echo $_SESSION[userid]; ?>//'><?php echo _t("View All") ?></a></div>
                                </div>
                            </div>
                        </div>
                        <!-- tab-block END -->
                    </div>
                </div>
                <!-- TAB END -->
                <!-- TAB -->
                <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="friends">
                    <div class="row">
                        <!-- tab-block -->
                        <div class="tab-block-col col">
                            <div class="tab-block-inner-col inner-col">
                                <div class="user-friends">
                                    <h2><? echo ucwords($urow['username']); ?>'s <?php echo _t("Friends") ?></h2>
                                    <div class="row">
                                        <? getUsersFriends($_SESSION['userid'], 8); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab-block END -->
                    </div>
                </div>
                <!-- TAB END -->
                <!-- TAB -->
                <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="wall">
                    <div class="row">
                        <!-- tab-block -->
                        <div class="tab-block-col col">
                            <div class="tab-block-inner-col inner-col">
                                <div class="user-wall">
                                    <h2><? echo ucwords($urow['username']); ?>'s <?php echo _t("Wall") ?></h2>
                                    <!-- comments -->
                                    <div class="row">
                                        <section class="comments-col col" id="comments">
                                            <div class="comments-inner-col inner-col">
                                                <?
                                                $contentID = $urow['record_num'];
                                                $commentsType = 2;
                                                //include($basepath . '/includes/inc.comments.php');
                                                include('widgets/widget.comments.php');
                                                ?> 
                                            </div>
                                        </section>
                                    </div>

                                    <!-- comments END -->
                                </div>
                            </div>
                        </div>
                        <!-- tab-block END -->
                    </div>
                </div>
                <!-- TAB END -->

                <!-- TABS GOES HERE END -->
            </div>
        </section>
        <!-- profile-content END -->

    </div>
</div>
<!-- profilepage END -->