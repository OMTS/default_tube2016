<section class="sticky-top">
    <header id="main-header">
        <div class="wrapper">

            <div class="row">
                <!-- logo -->
                <div class="logo-col col">
                    <div class="logo-inner-col inner-col">
                        <a href="<?php echo $basehttp; ?>" title="<?php echo $sitename; ?>"><img src="<?php echo $template_url; ?>/images/logo.png" alt="<?php echo _t("Home") ?> - <?php echo $sitename; ?>"></a>
                    </div>
                </div>
                <!-- logo END -->

                <div class="nav-trigger-col col">
                    <button id="nav-trigger" class="nav-btn-trigger btn-trigger" data-trigger="nav">
                        <span class="icon i-navicon"></span>
                    </button>
                </div>
                <div class="search-trigger-col col">
                    <button id="search-trigger" class="search-btn-trigger btn-trigger" data-trigger="search">
                        <span class="icon i-search"></span>
                    </button>
                </div>

                <!-- search -->
                <div class="search-col col">
                    <button class="close-btn"><span class="icon i-close"></span></button>
                    <div class="search-inner-col inner-col">
                        <form action="<?php echo $basehttp; ?>/searchgate.php" method="get" >
                            <div class="search-wrapper">
                                <input type="text" placeholder="<?php echo _t("Search") ?>" value="<?php echo str_replace("-", " ", $_GET['q']); ?><?php echo str_replace("-", " ", $_GET['letter']); ?>" name="q" class="">
                                <select name="type" class="selectpicker" id="search-select" data-style="btn-selectpicker">
                                    <option <?php if ($_GET['type'] != "videos" && $_GET['type'] != "photos" && $_GET['type'] != "members") { ?>selected<?php } ?> value="" data-content="<span class='icon i-all'></span><span class='sub-label'><?php echo _t("All") ?></span>"><?php echo _t("All") ?></option>
                                    <option <?php if ($_GET['mode'] == "search" && $_GET['type'] == "videos") { ?>selected<?php } ?> value="videos" data-content="<span class='icon i-video'></span><span class='sub-label'><?php echo _t("Videos") ?></span>"><?php echo _t("Videos") ?></option>
                                    <option <?php if ($_GET['mode'] == "search" && $_GET['type'] == "photos") { ?>selected<?php } ?> value="photos" data-content="<span class='icon i-photo'></span><span class='sub-label'><?php echo _t("Images") ?></span>"><?php echo _t("Images") ?></option>
                                    <option <?php if ($_GET['mode'] == "search" && $_GET['type'] == "members") { ?>selected<?php } ?> value="members" data-content="<span class='icon i-group'></span><span class='sub-label'><?php echo _t("Community") ?></span>"><?php echo _t("Community") ?></option>
									<option <?php if ($_GET['controller'] == "pornstars" && $_GET['letter']) { ?>selected<?php } ?> value="models" data-content="<span class='icon i-star'></span><span class='sub-label'><?php echo _t("Models") ?></span>"><?php echo _t("Models") ?></option>
                                </select>
                                <button type="submit" class="btn btn-default"><span class="icon i-search"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- search END -->


                <!-- lang -->
                <?php $langsNav = flags(); ?>
                <div class="lang-col col col-right">
                    <div class="lang-inner-col inner-col">
						<? if (is_array($langsNav['list']) && count($langsNav['list']) > 0) { ?>
							<button id="sLang" class="lang-select" aria-expanded="false" aria-haspopup="true" data-toggle="dropdown">
								<span class="flag-img flag-selected">
									<img src="<?php echo $basehttp; ?>/core/images/flags/<?php echo strtolower($langsNav['current']['iso']); ?>.png" alt="">
								</span>
								<span class="icon i-caret-down"></span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="sLang">
								<?php foreach ($langsNav['list'] as $item) { ?>
									<li>
										<a href="<?php echo $item['http']; ?>" title="">
											<span class="flag-img"><img src="<?php echo $basehttp; ?>/core/images/flags/<?php echo strtolower($item['iso']); ?>.png" alt=""></span>
											<span class="sub-desc"><?php echo $item['iso']; ?></span>
										</a>
									</li>
								<?php } ?>
							</ul>
						<?php } else { ?>
							<button id="sLang" class="lang-select">
								<span class="flag-img flag-selected">
									<img src="<?php echo $basehttp; ?>/core/images/flags/<?php echo strtolower($langsNav['current']['iso']); ?>.png" alt="">
								</span>
							</button>
						<?php } ?>
                    </div>
                </div>
                <!-- lang END -->


                <!-- dim -->
                <div class="dim-col col">
                    <div class="dim-inner-col inner-col">

                        <a href="<?php echo $basehttp; ?>/change-colors" title="<?php echo _t("Change colors") ?>" class="dim-link" data-mb="invert-colors"><span class="icon i-bulp"></span></a>
                    </div>
                </div>
                <!-- dim END -->

                <!-- ucp -->
                <div class="ucp-col col">
                    <div class="ucp-inner-col inner-col">

                        <?php if (!isLoggedIn()) { ?>
                            <ul class="guest-options-list">
                                <li><a href="<?php echo $basehttp; ?>/login" title="<?php echo _t("Login") ?>"><span class="icon i-lock"></span><span class="sub-label"><?php echo _t("Login") ?></span></a></li>
                                <li><a href="<?php echo $basehttp; ?>/signup" title="<?php echo _t("Signup") ?>"><span class="icon i-register"></span><span class="sub-label"><?php echo _t("Free Registration") ?></span></a></li>
                            </ul>
                        <?php } else { ?>
                            <button id="sUcp" class="ucp-select" aria-expanded="false" aria-haspopup="true" data-toggle="dropdown">
                                <span class="inner-wrapper">
                                    <span class="avatar">
                                        <span class="avatar-img">
                                            <?php echo getUserAvatar($_SESSION['userid']); ?>
                                        </span>
                                    </span>
                                    <span class="user-name">
                                        <?php echo $_SESSION['username']; ?>
                                    </span>
                                    <span class="icon i-caret-down"></span>
                                </span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="sUcp">
                                <li><a title="<?php echo _t("My profile") ?>" href="<?php echo $basehttp; ?>/my-profile"><span class="icon i-user"></span> <?php echo _t("My profile") ?></a></li>
                                <li><a title="<?php echo _t("Edit profile") ?>" href="<?php echo $basehttp; ?>/edit-profile"><span class="icon i-edit"></span> <?php echo _t("Edit profile") ?></a></li>
                                <li>
                                    <?php $countInbox = dbValue("SELECT COUNT(`record_num`) AS `count` FROM `mail` WHERE `to_user` = '{$_SESSION['userid']}' AND `recipient_deleted` = 0 AND `recipient_read` = 0", 'count'); ?>
                                    <a title="<?php echo _t("Messages") ?>" href="<?php echo $basehttp; ?>/mailbox/"><span class="icon i-email"></span> <?php echo _t("Messages") ?> (<?php echo $countInbox; ?>)</a>
                                </li>

                                <?php if ($allowAddFavorites) { ?>
                                    <li><a title="<?php echo _t("Favorites") ?>" href="<?php echo $basehttp; ?>/favorites/"><span class="icon i-heart"></span> <?php echo _t("Favorites") ?></a></li>
                                <?php } ?>

                                <li>
                                    <?php $countFriendsInvites = dbValue("SELECT COUNT(`record_num`) AS `count` FROM `friends` WHERE `friend` = '{$_SESSION['userid']}' AND `approved` = 0", 'count'); ?>
                                    <a title="<?php echo _t("Friends") ?>" href="<?php echo $basehttp; ?>/my-friends"><span class="icon i-group"></span> <?php echo _t("Friends") ?> (<?php echo $countFriendsInvites; ?>)</a>
                                </li>
								<? if($vodMode) { ?>
								<li>
                                    <?php $userNumTokens = billingGetUserTokens($_SESSION['userid']); ?>
                                    <a title="<?php echo _t("Buy Tokens") ?>" href="<?php echo $basehttp; ?>/buy-tokens"><span class="icon i-purchased"></span> <?php echo _t("Buy Tokens") ?> (<?php echo (int)$userNumTokens['tokens']; ?>)</a>
                                </li>
								<? } ?>
                                <li><a title="<?php echo _t("Logout") ?>" href="<?php echo $basehttp; ?>/logout"><span class="icon i-sign-out"></span> <?php echo _t("Logout") ?></a></li>
                            </ul>
                        <?php } ?>
                    </div>
                </div>
                <!-- ucp END -->


                <!-- upload -->
                <div class="upload-col col">
                    <div class="upload-inner-col inner-col">
                        <?php if ($_SESSION['userid']) { ?>
                            <a href="<?php echo $basehttp; ?>/upload" title="<?php echo _t("Upload Video") ?>" class="btn btn-default btn-sm"><?php echo _t("Upload Video") ?></a>
                            <a href="<?php echo $basehttp; ?>/upload?option=photo" title="<?php echo _t("Upload Gallery") ?>" class="btn btn-default btn-sm"><?php echo _t("Upload Gallery") ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $basehttp; ?>/login" title="<?php echo _t("Upload") ?>" class="btn btn-default btn-sm"><?php echo _t("Upload") ?></a>
                        <?php } ?>
                    </div>
                </div>
                <!-- upload END -->

            </div>

        </div>
    </header> 
    <section id="navigation" class="nav-sec">
        <button class="close-btn"><span class="icon i-close"></span></button>
        <div class="inner-nav-sec">
            <div class="wrapper">
                <div class="row">

                    <!-- MAIN NAV -->
                    <nav id="main-nav" class="main-nav-col col">
                        <div class="main-nav-inner-col inner-col">

                            <ul class="main-nav-list">


                                <li class="menu-el<?php if (checkNav('index')) echo ' current active'; ?>"><a href="<?php echo $basehttp; ?>" title="<?php echo _t("Home") ?>"><span class="icon i-home"></span><span class="sub-label"><?php echo _t("Home") ?></span></a></li>
                                <li data-mb="expand-mobile" class="menu-el has-list plain-list<?php if (checkNav('videos')) echo ' current active'; ?>">
                                    <a href="<?php echo $basehttp; ?>/videos/" title="<?php echo _t("Movies") ?>"><span class="icon i-video"></span><span class="sub-label"><?php echo _t("Movies") ?></span><span class="icon i-caret-down"></span></a>
                                    <ul>
                                        <li><a href="<?php echo $basehttp; ?>/videos/" title="<?php echo _t("Most Recent") ?>"><?php echo _t("Most Recent") ?></a></li>
										<li><a href="<?php echo $basehttp; ?>/most-viewed/" title="<?php echo _t("Most Viewed") ?>"><?php echo _t("Most Viewed") ?></a></li>
                                        <li><a href="<?php echo $basehttp; ?>/top-rated/" title="<?php echo _t("Top Rated") ?>"><?php echo _t("Top Rated") ?></a></li>
                                        <li><a href="<?php echo $basehttp; ?>/most-discussed/" title="<?php echo _t("Most Discussed") ?>"><?php echo _t("Most Discussed") ?></a></li>
                                        <li><a href="<?php echo $basehttp; ?>/longest/" title="<?php echo _t("Longest") ?>"><?php echo _t("Longest") ?></a></li>
                                    </ul>
                                </li>
                                <li data-mb="expand-mobile" class="menu-el has-list plain-list<?php if (checkNav('photos')) echo ' current active'; ?>">
                                    <a href="<?php echo $basehttp; ?>/photos/" title="<?php echo _t("Albums") ?>"><span class="icon i-photo"></span><span class="sub-label"><?php echo _t("Albums") ?></span><span class="icon i-caret-down"></span></a>
                                    <ul>
                                        <li><a href="<?php echo $basehttp; ?>/photos/" title="<?php echo _t("Most Recent") ?>"><?php echo _t("Most Recent") ?></a></li>
										<li><a href="<?php echo $basehttp; ?>/photos/most-viewed/" title="<?php echo _t("Most Viewed") ?>"><?php echo _t("Most Viewed") ?></a></li>
                                        <li><a href="<?php echo $basehttp; ?>/photos/top-rated/" title="<?php echo _t("Top Rated") ?>"><?php echo _t("Top Rated") ?></a></li>
                                        <li><a href="<?php echo $basehttp; ?>/photos/most-discussed/" title="<?php echo _t("Most Discussed") ?>"><?php echo _t("Most Discussed") ?></a></li>
                                    </ul>
                                </li>
								<li class="menu-el<?php if ($_GET['mode'] == 'vr') echo ' current active'; ?>"><a href="<?php echo $basehttp; ?>/vr/" title="<?php echo _t("VR/360°") ?>"><span class="icon i-compas"></span><span class="sub-label"><?php echo _t("VR/360°") ?></span></a></li>
                                <li data-mb="expand-mobile" class="menu-el has-list<?php if (checkNav('channels') || $_GET['mode'] == 'channel') echo ' current active'; ?>">
                                    <a href="<?php echo $basehttp; ?>/channels/" title="<?php echo _t("Categories") ?>" data-load="categories"><span class="icon i-categories"></span><span class="sub-label"><?php echo _t("Categories") ?></span><span class="icon i-caret-down"></span></a>
                                    <div class="extended-categories">
                                        <div class="wrapper">
                                            <div class="row">

                                                <?php showChannelsBlocks(array('type' => 'navigation', 'limit' => 8)); ?>

                                                <!-- cat-item -->
                                                <div class="cat-item-col cat-item--more item-col item--channel col">
                                                    <div class="cat-item-inner-col inner-col">
                                                        <a href="<?php echo $basehttp . "/channels/"; ?>" title="<?php echo _t("See All") ?>">
                                                            <span class="image">
                                                                <span class="icon i-plus"></span>
                                                            </span>
                                                            <span class="item-info">
                                                                <span class="title">
                                                                    <?php echo _t("See All") ?>
                                                                </span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <!-- cat-item END -->
                                            </div>
                                        </div>

                                    </div>
                                </li>
								
                                <li class="menu-el<?php if (checkNav('pornstars')) echo ' current active'; ?>"><a href="<?php echo $basehttp; ?>/models/" title="<?php echo _t("Models") ?>"><span class="icon i-star"></span><span class="sub-label"><?php echo _t("Models") ?></span></a></li>
                                <li class="menu-el<?php if (checkNav('members')) echo ' current active'; ?>"><a href="<?php echo $basehttp; ?>/members/" title="<?php echo _t("Community") ?>"><span class="icon i-group"></span><span class="sub-label"><?php echo _t("Community") ?></span></a></li>
                                <li class="menu-el<?php if (checkNav('paysites')) echo ' current active'; ?>"><a href="<?php echo $basehttp; ?>/paysites/" title="<?php echo _t("Paysites") ?>"><span class="icon i-money"></span><span class="sub-label"><?php echo _t("Paysites") ?></span></a></li>
								<li class="menu-el<?php if (checkNav('tags')) echo ' current active'; ?>"><a href="<?php echo $basehttp; ?>/tags" title="<?php echo _t("Tags") ?>"><span class="icon i-tags"></span><span class="sub-label"><?php echo _t("Tags") ?></span></a></li>
								<? if($_SESSION[userid] && $vodMode) { ?>
								<li class="menu-el<?php if (checkNav('purchased')) echo ' current active'; ?>"><a href="<?php echo $basehttp; ?>/my-rentals/" title="<?php echo _t("Tags") ?>"><span class="icon i-purchased"></span><span class="sub-label"><?php echo _t("My Rentals") ?></span></a></li>
								<? } ?>
                            </ul>
                        </div>
                    </nav>
                    <!-- MAIN NAV END -->
                </div>
            </div>
        </div>
    </section>
</section>