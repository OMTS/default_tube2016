<?php $colorClass = ($_COOKIE["mbColorScheme"] == 1) ? " inverted-colors" : ""; ?><!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7<?php echo $colorClass ?>"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie-7-only<?php echo $colorClass ?>"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie-8-only<?php echo $colorClass ?>"><![endif]-->
<!--[if gte IE 9]><!--> <html class="no-js no-filter<?php echo $colorClass ?>"><!--<![endif]-->
    <head>
        <?php getWidget('widget.header_scripts.php'); ?>
    </head>
    <body class="page-<?php echo bodyClasses(); ?>">
        <?php getWidget('widget.network.php'); ?>

        <section class="page-wrap">
            <?php getTemplate('template.nav.php'); ?>
            <?php getWidget('widget.ad_top.php'); ?>

            <section class="content-sec">
                <div class="wrapper">
                    <?php getTemplate('template.notifications.php'); ?>
                    <div class="row">

                        <!-- content -->
                        <div class="content-col col">
                            <div class="content-inner-col inner-col">
                                <?php if (($_GET[controller] == "index" || checkNav('paysites') || checkNav('channels')) && $_GET[mode] != "search") { ?>
                                    <?php getTemplate('template.sidebar.php'); ?>
                                <?php } ?>

                                <?php if ($_GET[controller] == "members" || ($_GET[mode] == "search" && $_GET[type] == "members")) { ?>
                                    <?php getTemplate('template.sidebar_members.php'); ?>
                                <?php } ?>

                                <!-- main -->
                                <main class="main-col">
                                    <div class="main-inner-col inner-col">
                                        <?php if (checkNav('index')) { ?>
                                            <!-- HOT Movie -->
                                            <div class="box-container">
                                                <div class="inner-box-container">
                                                    <!-- title -->
                                                    <header class="row">
                                                        <div class="title-col title-col--normal col">
                                                            <div class="title-inner-col inner-col">
                                                                <h1><?php echo _t("Being Watched Now") ?></h1>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <!-- title END -->

                                                    <div class="row">
                                                        <?php getWidget('widget.ad_list.php'); ?>
                                                        <?php showBeingWatched('template.content_item.php', 6, 'indexRandom'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- HOT PORN -->

                                            <!-- MOST VIEWED -->
                                            <div class="box-container">
                                                <div class="inner-box-container">
                                                    <!-- title -->
                                                    <header class="row">
                                                        <div class="title-col title-col--normal col">
                                                            <div class="title-inner-col inner-col">
                                                                <h1><?php echo _t("Most Viewed Videos") ?></h1>
                                                                <a href="<?php echo $basehttp; ?>/most-viewed/" title="<?php echo _t("Most Viewed") ?>" class="btn btn-header"><?php echo _t("Most Viewed") ?></a>
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <!-- title END -->
                                                    <div class="row">
                                                        <?php _showMostViewed('template.content_item.php', 10); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- MOST VIEWED END -->
                                        <?php } ?>

                                        <?php getTemplate('template.paysite_header.php'); ?>

                                        <!-- STANDARD CONTENT -->
                                        <div class="box-container">
                                            <div class="inner-box-container">

                                                <!-- title -->
                                                <header class="row">
                                                    <div class="title-col title-col--normal col">
                                                        <div class="title-inner-col inner-col">
                                                            <h1><?php echo $headertitle; ?> <?php if (checkNav('index')) { ?><span class="dimmed-desc">(<?php echo showVideosCounter(); ?>)</span><?php } ?></h1>
                                                            <?php if (checkNav('index') || checkNav('most-viewed') || checkNav('top-rated') || checkNav('most-recent')) { ?>
                                                                <!-- filter-sm -->
                                                                <div class="filter-sm-col col">
                                                                    <div class="filter-sm-inner-col inner-col">
                                                                        <div class="filter-sort">

                                                                            <div class="fake-filter">
                                                                                <?
                                                                                $fake_select = _t("- select -");
                                                                                if (checkNav('most-recent')) {
                                                                                    $fake_select = _t("Newest");
                                                                                }
                                                                                if (checkNav('top-rated')) {
                                                                                    $fake_select = _t("Top Rated");
                                                                                }
                                                                                if (checkNav('most-viewed')) {
                                                                                    $fake_select = _t("Most Viewed");
                                                                                }
                                                                                ?>
                                                                                <div aria-expanded="false" class="fake-selected btn-selected btn btn-header" data-toggle="dropdown"><?php echo $fake_select; ?></div>
                                                                                <ul class="fake-select-list dropdown dropdown-menu dropdown-menu-right">
                                                                                    <li><a title="<?php echo _t("Newest") ?>" href="<?php echo $basehttp; ?>/most-recent/"><?php echo _t("Newest") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Most Viewed") ?>" href="<?php echo $basehttp; ?>/most-viewed/"><?php echo _t("Most Viewed") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Top Rated") ?>" href="<?php echo $basehttp; ?>/top-rated/"><?php echo _t("Top Rated") ?></a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- filter-sm END -->
                                                            <?php } ?>

                                                            <?php if ($_GET[controller] == "index" && $_GET['mode'] != 'uploads-by-user'  && $_GET['mode'] != "channel" && $_GET['mode'] != "search" && $_GET['mode'] != "paysites" && $_GET['mode'] != 'photos') { ?>
                                                                <?php // filtry po dacie - widoczne na podstronach z wyjatkiem HOME i CHANNELS i SEARCH i PAYSITES ?>
                                                                <!-- filter-sm -->
                                                                <div class="filter-sm-col col">
                                                                    <div class="filter-sm-inner-col inner-col">
                                                                        <div class="filter-sort">

                                                                            <div class="fake-filter">
                                                                                <?
                                                                                $fake_select2 = _t("All time");
                                                                                if ($_GET['dateRange'] == 'day') {
                                                                                    $fake_select2 = _t("Today");
                                                                                }
                                                                                if ($_GET['dateRange'] == 'week') {
                                                                                    $fake_select2 = _t("Last 7 days");
                                                                                }
                                                                                if ($_GET['dateRange'] == 'month') {
                                                                                    $fake_select2 = _t("Last 30 days");
                                                                                }

                                                                                if ($_GET['mode']) {
                                                                                    $modeUrl = $_GET['mode'];
                                                                                } else {
                                                                                    $modeUrl = "most-recent";
                                                                                }
                                                                                
                                                                                if (is_numeric($_GET['user'])) {
                                                                                    $modeUrl .= '/' . (int) $_GET['user'];
                                                                                }
                                                                                ?>
                                                                                <div aria-expanded="false" class="fake-selected btn-selected btn btn-header" data-toggle="dropdown"><?php echo $fake_select2; ?></div>
                                                                                <ul class="fake-select-list dropdown dropdown-menu dropdown-menu-right">
                                                                                    <li><a title="<?php echo _t("All Time") ?>" href="<?php echo $basehttp; ?>/<?php echo $modeUrl; ?>/"><?php echo _t("All time") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Today") ?>" href="<?php echo $basehttp; ?>/<?php echo $modeUrl; ?>/day/"><?php echo _t("Today") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Last 7 days") ?>" href="<?php echo $basehttp; ?>/<?php echo $modeUrl; ?>/week/"><?php echo _t("Last 7 days") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Last 30 days") ?>" href="<?php echo $basehttp; ?>/<?php echo $modeUrl; ?>/month/"><?php echo _t("Last 30 days") ?></a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- filter-sm END -->
                                                            <?php } ?>
                                                            <?php if ($_GET['mode'] == 'channel' && is_numeric($_GET['channel'])) { ?>
                                                                <?php // SORTOWANIE TYLKO W CHANNELS ?>
                                                                <!-- filter-sm -->
                                                                <div class="filter-sm-col col">
                                                                    <div class="filter-sm-inner-col inner-col">
                                                                        <div class="filter-sort">

                                                                            <div class="fake-filter">
                                                                                <?
                                                                                $fake_select3 = _t("Newest");
                                                                                if ($_GET[sortby] == 'length') {
                                                                                    $fake_select3 = _t("Longest");
                                                                                }
                                                                                if ($_GET[sortby] == 'rating') {
                                                                                    $fake_select3 = _t("Top Rated");
                                                                                }
                                                                                if ($_GET[sortby] == 'views') {
                                                                                    $fake_select3 = _t("Most Viewed");
                                                                                }
                                                                                ?>
                                                                                <div aria-expanded="false" class="fake-selected btn-selected btn btn-header" data-toggle="dropdown"><?php echo $fake_select3; ?></div>
                                                                                <ul class="fake-select-list dropdown dropdown-menu dropdown-menu-right">
                                                                                    <li><a title="<?php echo _t("Newest") ?>" href="<?php echo generateUrl('channel', $title, $_GET['channel']); ?>"><?php echo _t("Newest") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Longest") ?>" href="<?php echo generateUrl('channel', $title, $_GET['channel']); ?>longest/"><?php echo _t("Longest") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Top Rated") ?>" href="<?php echo generateUrl('channel', $title, $_GET['channel']); ?>rating/"><?php echo _t("Top Rated") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Most Viewed") ?>" href="<?php echo generateUrl('channel', $title, $_GET['channel']); ?>views/"><?php echo _t("Most Viewed") ?></a></li>

                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- filter-sm END -->
                                                            <?php } ?>
															<?php if ($_GET['mode'] == 'photos') { ?>
                                                                <?php // SORTOWANIE TYLKO W CHANNELS ?>

                                                                <!-- filter-sm -->
                                                                <div class="filter-sm-col col">
                                                                    <div class="filter-sm-inner-col inner-col">
                                                                        <div class="filter-sort">

                                                                            <div class="fake-filter">
                                                                                <?
                                                                                $fake_selectphoto = _t("Newest");
                                                                                if ($_GET['sortby'] == 'most-discussed') {
                                                                                    $fake_selectphoto = _t("Most Discussed");
                                                                                }
                                                                                if ($_GET['sortby'] == 'top-rated') {
                                                                                    $fake_selectphoto = _t("Top Rated");
                                                                                }
                                                                                if ($_GET['sortby'] == 'most-viewed') {
                                                                                    $fake_selectphoto = _t("Most Viewed");
                                                                                }
                                                                                ?>
                                                                                <div aria-expanded="false" class="fake-selected btn-selected btn btn-header" data-toggle="dropdown"><?php echo $fake_selectphoto; ?></div>
                                                                                <ul class="fake-select-list dropdown dropdown-menu dropdown-menu-right">
                                                                                    <li><a title="<?php echo _t("Newest") ?>" href="<? echo $basehttp; ?>/photos/"><?php echo _t("Newest") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Top Rated") ?>" href="<? echo $basehttp; ?>/photos/top-rated/"><?php echo _t("Top Rated") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Most Viewed") ?>" href="<? echo $basehttp; ?>/photos/most-viewed/"><?php echo _t("Most Viewed") ?></a></li>
																					<li><a title="<?php echo _t("Most Discussed") ?>" href="<? echo $basehttp; ?>/photos/most-discussed/"><?php echo _t("Most Discussed") ?></a></li>

                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- filter-sm END -->
                                                            <?php } ?>
                                                            <?php if ($_GET['controller'] == 'pornstars') { ?>
                                                                <?php // SORTOWANIE TYLKO PORNSTARS  ?>
                                                                <!-- filter-sm -->
                                                                <div class="filter-sm-col col">
                                                                    <div class="filter-sm-inner-col inner-col">
                                                                        <div class="filter-sort">

                                                                            <div class="fake-filter">
                                                                                <?
                                                                                $fake_select4 = _t("Alphabetical");
                                                                                if ($_GET[sortby] == 'rating') {
                                                                                    $fake_select4 = _t("Top Rated");
                                                                                }
                                                                                $letter = "";
                                                                                if ($_GET['letter']) {
                                                                                    $letter = $_GET['letter'] . "/";
                                                                                }
                                                                                ?>
                                                                                <div aria-expanded="false" class="fake-selected btn-selected btn btn-header" data-toggle="dropdown"><?php echo $fake_select4; ?></div>
                                                                                <ul class="fake-select-list dropdown dropdown-menu dropdown-menu-right">
                                                                                    <li><a title="<?php echo _t("Alphabetical") ?>" href="<?php echo $basehttp; ?>/models/<?php echo $letter; ?>"><?php echo _t("Alphabetical") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Top Rated") ?>" href="<?php echo $basehttp; ?>/models/<?php echo $letter; ?>rating/"><?php echo _t("Top Rated") ?></a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- filter-sm END -->
                                                            <?php } ?>

                                                            <?php if ($_GET['mode'] == 'search' && $_GET['type'] != 'members') { ?>
                                                                <?php // SORTOWANIE TYLKO W CHANNELS  ?>
                                                                <!-- filter-sm -->
                                                                <div class="filter-sm-col col">
                                                                    <div class="filter-sm-inner-col inner-col">
                                                                        <div class="filter-sort">

                                                                            <div class="fake-filter">
                                                                                <?
                                                                                $fake_select5 = _t("Most Relevant");
                                                                                if ($_GET[sortby] == 'length') {
                                                                                    $fake_select5 = _t("Longest");
                                                                                }
                                                                                if ($_GET[sortby] == 'rating') {
                                                                                    $fake_select5 = _t("Top Rated");
                                                                                }
                                                                                if ($_GET[sortby] == 'views') {
                                                                                    $fake_select5 = _t("Most Viewed");
                                                                                }
																				if ($_GET[sortby] == 'newest') {
                                                                                    $fake_select5 = _t("Newest");
                                                                                }

                                                                                $q = $_GET[q] . "/";
                                                                                $t = "";
                                                                                if ($_GET[type]) {
                                                                                    $t = $_GET[type] . "/";
                                                                                }
                                                                                ?>
                                                                                <div aria-expanded="false" class="fake-selected btn-selected btn btn-header" data-toggle="dropdown"><?php echo $fake_select5; ?></div>
                                                                                <ul class="fake-select-list dropdown dropdown-menu dropdown-menu-right">
																					<li><a title="<?php echo _t("Most Relevant") ?>" href="<?php echo $basehttp . "/search/" . $t . $q; ?>"><?php echo _t("Most Relevant") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Newest") ?>" href="<?php echo $basehttp . "/search/" . $t . $q; ?>newest/"><?php echo _t("Newest") ?></a></li>
                                                                                    <?php if ($_GET[type] == 'videos') { ?><li><a title="<?php echo _t("Longest") ?>" href="<?php echo $basehttp . "/search/" . $t . $q . "longest/"; ?>"><?php echo _t("Longest") ?></a></li><?php } ?>
                                                                                    <li><a title="<?php echo _t("Top Rated") ?>" href="<?php echo $basehttp . "/search/" . $t . $q . "rating/"; ?>"><?php echo _t("Top Rated") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Most Viewed") ?>" href="<?php echo $basehttp . "/search/" . $t . $q . "views/"; ?>"><?php echo _t("Most Viewed") ?></a></li>

                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- filter-sm END -->
                                                            <?php } ?>

                                                            <?php if ($_GET['mode'] == 'paysites' && is_numeric($_GET['paysite'])) { ?>
                                                                <?php // SORTOWANIE TYLKO W PAYSTIES ?>
                                                                <!-- filter-sm -->
                                                                <div class="filter-sm-col col">
                                                                    <div class="filter-sm-inner-col inner-col">
                                                                        <div class="filter-sort">

                                                                            <div class="fake-filter">
                                                                                <?
                                                                                $fake_select6 = _t("Newest");
                                                                                if ($_GET[sortby] == 'length') {
                                                                                    $fake_select6 = _t("Longest");
                                                                                }
                                                                                if ($_GET[sortby] == 'rating') {
                                                                                    $fake_select6 = _t("Top Rated");
                                                                                }
                                                                                if ($_GET[sortby] == 'views') {
                                                                                    $fake_select6 = _t("Most Viewed");
                                                                                }
                                                                                ?>
                                                                                <div aria-expanded="false" class="fake-selected btn-selected btn btn-header" data-toggle="dropdown"><?php echo $fake_select6; ?></div>
                                                                                <ul class="fake-select-list dropdown dropdown-menu dropdown-menu-right">
                                                                                    <li><a title="<?php echo _t("Newest") ?>" href="<?php echo generateUrl('paysite', $name, $_GET['paysite']); ?>"><?php echo _t("Newest") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Longest") ?>" href="<?php echo generateUrl('paysite', $name, $_GET['paysite']); ?>longest/"><?php echo _t("Longest") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Top Rated") ?>" href="<?php echo generateUrl('paysite', $name, $_GET['paysite']); ?>rating/"><?php echo _t("Top Rated") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Most Viewed") ?>" href="<?php echo generateUrl('paysite', $name, $_GET['paysite']); ?>views/"><?php echo _t("Most Viewed") ?></a></li>

                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- filter-sm END -->
                                                            <?php } ?>

                                                            <?php if ($_GET[controller] == "members" || ($_GET[mode] == "search" && $_GET[type] == "members")) { ?>
                                                                <?php // SORTOWANIE TYLKO W MEMBERS ?>
                                                                <!-- filter-sm -->
                                                                <div class="filter-sm-col col">
                                                                    <div class="filter-sm-inner-col inner-col">
                                                                        <div class="filter-sort">

                                                                            <div class="fake-filter">
                                                                                <?
                                                                                $fake_select7 = _t("Alphabetical");

                                                                                if ($_SESSION['ms']['order'] == 'ORDER BY date_joined') {
                                                                                    $fake_select7 = _t("Join Date");
                                                                                } else {
                                                                                    $fake_select7 = _t("Alphabetical");
                                                                                }
                                                                                ?>
                                                                                <div aria-expanded="false" class="fake-selected btn-selected btn btn-header" data-toggle="dropdown"><?php echo $fake_select7; ?></div>
                                                                                <ul class="fake-select-list dropdown dropdown-menu dropdown-menu-right">
                                                                                    <li><a title="<?php echo _t("Alphabetical") ?>" href="<?php echo $basehttp; ?>/members/?sortby=alphabetical"><?php echo _t("Alphabetical") ?></a></li>
                                                                                    <li><a title="<?php echo _t("Join Date") ?>" href="<?php echo $basehttp; ?>/members/?sortby=date_joined"><?php echo _t("Join Date") ?></a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- filter-sm END -->
                                                            <?php } ?>

                                                        </div>
														
                                                    </div>
													
                                                </header>
												<? if($config['_metatags']['catdesc']) { echo "<p>".$config['_metatags']['catdesc']."</p>"; } ?>
                                                <!-- title END -->
                                                <div class="row">

                                                    <?php if (checkNav('pornstars')) { ?>
                                                        <?php include($template_path . '/template.pornstar_alphabet.php'); ?>
                                                    <?php } ?>

                                                    <!-- HEADER UP -->
                                                    <!-- HEADER UP END -->