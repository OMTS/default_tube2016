<?php
if ($_GET['controller'] == 'index' && isset($_GET['mode']) && $_GET['mode'] == 'paysites' && is_numeric($_GET['paysite'])) {
    if ($currentLang) {
        $langSelect = ", paysites_languages.data";
        $langJoin = " RIGHT JOIN paysites_languages ON paysites_languages.paysite = paysites.record_num";
        $langWhere = " AND paysites_languages.language = '$currentLang'";
    }

    $getPaysite = dbQuery("SELECT * $langSelect FROM paysites $langJoin WHERE enabled = 1 AND record_num = {$_GET['paysite']} $langWhere", false);
    if (!empty($getPaysite)) {
        $paysite = $getPaysite[0];
        if ($currentLang) {
            $langData = unserialize($paysite['data']);
            $description = $langData['description'];
        } else {
            $description = $paysite['description'];
        }
        ?>
        <?php if (file_exists("$misc_path/paysite{$paysite['record_num']}.jpg") || !empty($description)) { ?>
            <div class="box-container">
                <div class="inner-box-container">
                    <div class="paysite-wrapper offset-columns clear">
                        <aside class="aside-tabs-col">
                            <div class="aside-tabs-inner-col inner-col">
                                <div class="profile-img-avatar">
                                    <?php if (file_exists("$misc_path/paysite{$paysite['record_num']}.jpg")) { ?>
                                        <img  alt="<? echo $paysite['name']; ?>" src="<?php echo "$misc_url/paysite{$paysite['record_num']}.jpg"; ?>">
                                    <?php } else { ?>
                                        <img src="<? echo $basehttp; ?>/core/images/catdefault.jpg" alt="<? echo $paysite['name']; ?>" >
                                    <?php } ?>
                                </div>
                            </div>
                        </aside>
                        <!-- paysite-description -->
                        <section class="paysite-description-col ">
                            <div class="paysite-description-inner-col inner-col">
                                <?php echo $description; ?>
                            </div>
                        </section>
                        <!-- paysite-description END -->
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
<?php } ?>