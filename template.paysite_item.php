<?
$link = generateUrl('paysite', $row['name'], $row['record_num']);
?>
<!-- item -->
<div class="item-col item--paysite col">
    <div class="item-inner-col inner-col">
        <a href="<? echo $link; ?>" title="<? echo htmlentities($row['name'], ENT_QUOTES, 'UTF-8'); ?>">
            <span class="image">
                <? if (file_exists($misc_path . '/paysite' . $row['record_num'] . '.jpg')) { ?>
                    <img src="<? echo $misc_url; ?>/paysite<? echo $row['record_num']; ?>.jpg" alt="<? echo htmlentities($row['name'], ENT_QUOTES, 'UTF-8'); ?>">
                <? } else { ?>
                    <img src="<? echo $basehttp; ?>/core/images/catdefault.jpg" alt="<? echo htmlentities($row['name'], ENT_QUOTES, 'UTF-8'); ?>">
                <? } ?>
            </span>
            <span class="item-info">
                <span class="title"><? echo $row['name']; ?></span>
            </span>
        </a>
    </div>
</div>
<!-- item END -->