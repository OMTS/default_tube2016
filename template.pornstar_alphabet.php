<? 
    $sortprefix = "";
    if($_GET['sortby']) {
        $sortprefix = $_GET['sortby']."/";
    }
?>
<!-- alphabet -->
<div class="alphabet-col col col-full">
    <div class="alphabet-inner-col inner-col">
        <a class="alphabet-letter<? if (!$_GET['letter']) {
    echo ' active';
} ?>" href="/models/<? echo $sortprefix; ?>" >All</a> 
        <?
        $alphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
        foreach ($alphabet as $i) {
            ?>
            <a class="alphabet-letter<? if ($i == $_GET['letter']) {
            echo ' active';
        } ?>" href="/models/<? echo $i; ?>/<? echo $sortprefix; ?>" ><? echo ucwords($i); ?></a> 
<? } ?>
    </div>
</div>
<!-- alphabet END -->