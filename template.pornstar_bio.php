<!-- profilepage -->
<div class="profilepage-col col">
    <div class="profilepage-inner-col offset-columns inner-col">


        <!-- aside-tabs -->
        <aside class="aside-tabs-col">
            <div class="aside-tabs-inner-col inner-col">
                <div class="profile-img-avatar">
                    <?php if ($rrow['thumb'] != '') { ?>
                        <img src="<?php echo $basehttp; ?>/media/misc/<?php echo $rrow['thumb']; ?>" />
                    <?php } else { ?>
                        <img src='<?php echo $basehttp; ?>/core/images/avatar.jpg' height="<?php echo $thumbheight; ?>" width="<?php echo $thumbwidth; ?>" alt="<?php echo ucwords($rrow['username']); ?>" />
                    <?php } ?>
                </div>
                <?php include($basepath . '/includes/rating/templateTh.php'); ?>
                <div class="rating-desc mini-text">
                    <?php $count_votes = dbQuery("SELECT total_votes FROM ratings WHERE content = '$rrow[record_num]' AND type = '1'"); ?>
                    <?php if (is_array($count_votes)) { ?>
                        <?php $count_votes = $count_votes[0]['total_votes']; ?>
                    <?php } else { ?>
                        <?php $count_votes = '0'; ?>
                    <?php } ?>
                </div>
                <ul class="tabs-list tabs-list--aside">
                    <li class="active"><a data-mb="tab" data-opt-tab="information" title="<?php echo _t("Profile") ?>" href="#"><span class="icon i-info"></span><span class="sub-label"><?php echo _t("Profile") ?></span></a></li>
                    <li><a data-mb="tab" data-opt-tab="uploads_v" title="<?php echo _t("Videos") ?>" href="#"><span class="icon i-video"></span><span class="sub-label"><?php echo _t("Videos") ?></span></a></li>
                    <li><a data-mb="tab" data-opt-tab="uploads_p" title="<?php echo _t("Photos") ?>" href="#"><span class="icon i-photo"></span><span class="sub-label"><?php echo _t("Photos") ?></span></a></li>
                    <li><a data-mb="tab" data-opt-tab="comments" title="<?php echo _t("Wall") ?>" href="#"><span class="icon i-wall"></span><span class="sub-label"><?php echo _t("Comments") ?></span></a></li>
                </ul>
            </div>
        </aside>
        <!-- aside-tabs END -->

        <!-- profile-content -->
        <section class="profile-content-col">
            <div class="profile-content-inner-col inner-col">
                <!-- TABS GOES HERE -->
                <!-- TAB -->
                <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="information">
                    <div class="row">
                        <!-- tab-block -->
                        <div class="tab-block-col col">
                            <div class="tab-block-inner-col inner-col">
                                <div class="profile-info">
                                    <div class="d-container">
                                        <h2><?php echo _t("Profile") ?></h2>
                                        <ul class="profile-list">
                                            <li class="profile-field profile-field-name"><span class="sub-label"><?php echo _t("Name"); ?>:</span> <span class="sub-desc"><?php echo $rrow['name']; ?></span></li>
                                            <li class="profile-field profile-field-ska"><span class="sub-label"><?php echo _t("Aka"); ?>:</span> <span class="sub-desc"><?php echo $rrow['aka']; ?></span></li>
                                            <li class="profile-field profile-field-fob"><span class="sub-label"><?php echo _t("Dob"); ?>:</span> <span class="sub-desc"><?php echo $rrow['dob']; ?></span></li>
                                            <li class="profile-field profile-field-height"><span class="sub-label"><?php echo _t("Height"); ?>:</span> <span class="sub-desc"><?php echo $rrow['height']; ?></span></li>
                                            <li class="profile-field profile-field-weight"><span class="sub-label"><?php echo _t("Weight"); ?>:</span> <span class="sub-desc"><?php echo $rrow['weight']; ?></span></li>
                                            <li class="profile-field profile-field-measure"><span class="sub-label"><?php echo _t("Measurements"); ?>:</span> <span class="sub-desc"><?php echo $rrow['measurements']; ?></span></li>
                                            <?php $custom = unserialize($rrow['custom']); ?>
                                            <?php foreach ($custom_pornstar_fields as $k => $v) { ?>
                                                <?php if ($custom[$k]) { ?>
                                                    <li class="profile-field profile-field-custom"><span class="sub-label"><?php echo $k; ?>:</span> <span class="sub-desc"><?php echo $custom[$k]; ?></span></li>
                                                <?php } ?>
                                            <?php } ?> 
                                        </ul>
                                        <ul class="profile-list">
                                            <li class="profile-field profile-field-hair"><span class="sub-label"><?php echo _t("Hair"); ?>:</span> <span class="sub-desc"><?php echo $rrow['hair']; ?></span></li>
                                            <li class="profile-field profile-field-eyes"><span class="sub-label"><?php echo _t("Eyes"); ?>:</span> <span class="sub-desc"><?php echo $rrow['eyes']; ?></span></li>
                                            <li class="profile-field profile-field-ethnicity"><span class="sub-label"><?php echo _t("Ethnicity"); ?>:</span> <span class="sub-desc"><?php echo $rrow['ethnicity']; ?></span></li>
                                            <li class="profile-field profile-field-site"><span class="sub-label"><?php echo _t("Official site"); ?>:</span> <span class="sub-desc"><a href="<?php echo $rrow['official_site_url']; ?>" target="_blank"> <?php echo $rrow['official_site_name']; ?></a></span></li>
                                            <li class="profile-field profile-field-biography"><span class="sub-label"><?php echo _t("Biography"); ?>:</span> <span class="sub-desc"><?php echo $rrow['biography']; ?></span></li>
                                            <li class="profile-field profile-field-rank"><span class="sub-label"><?php echo _t("Rank"); ?>:</span> <span class="sub-desc"><?php echo $rrow['rank']; ?></span></li>
                                            <li class="profile-field profile-field-views"><span class="sub-label"><?php echo _t("Views"); ?>:</span> <span class="sub-desc"><?php echo $rrow['views']; ?></span></li>
                                        </ul>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab-block END -->
                    </div>
                </div>
                <!-- TAB END -->
                <!-- TAB -->
                <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="uploads_v">
                    <div class="row">
                        <!-- tab-block -->
                        <div class="tab-block-col col">
                            <div class="tab-block-inner-col inner-col">
                                <div class="recent-uploads">
                                    <h2><?php echo $rrow['name']; ?>'s <?php echo _t("Videos") ?></h2>
                                    <div class="row">
                                        <?php getModelsContent($rrow['record_num'], 0); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab-block END -->
                    </div>
                </div>
                <!-- TAB END -->
                <!-- TAB -->
                <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="uploads_p">
                    <div class="row">
                        <!-- tab-block -->
                        <div class="tab-block-col col">
                            <div class="tab-block-inner-col inner-col">
                                <div class="recent-uploads">
                                    <h2><?php echo $rrow['name']; ?>'s <?php echo _t("Photos") ?></h2>
                                    <div class="row">
                                        <?php getModelsContent($rrow['record_num'], 1); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- tab-block END -->
                    </div>
                </div>
                <!-- TAB END -->
                <!-- TAB -->
                <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="comments">
                    <div class="row">
                        <!-- tab-block -->
                        <div class="tab-block-col col">
                            <div class="tab-block-inner-col inner-col">
                                <div class="recent-uploads">
                                    <h2><?php echo $rrow['name']; ?>'s <?php echo _t("Comments") ?></h2>
                                    <?php $contentID = $rrow['record_num']; ?>
                                    <?php $commentsType = 1; ?>
                                    <?php include('widgets/widget.comments.php'); ?>
                                </div>
                            </div>
                        </div>
                        <!-- tab-block END -->
                    </div>
                </div>
                <!-- TAB END -->

                <!-- TABS GOES HERE END -->
            </div>
        </section>
        <!-- profile-content END -->

    </div>
</div>
<!-- profilepage END -->