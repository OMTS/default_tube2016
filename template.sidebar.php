<!-- aside-main -->
<aside class="aside-main-col" data-mb="aside" data-opt-filters-on="Show filters" data-opt-filters-off="Hide filters">
    <div class="aside-main-inner-col inner-col">
        <!-- FILTER -->
        <div class="box-container">
            <div class="inner-box-container">
                <?php if ((($_GET['controller'] == "index" || (checkNav('channels') && is_numeric($_GET['channel']))) && $_GET['mode'] != "search")) { ?>
                    <div class="filter-container">
                        <!-- title -->
                        <header class="row">
                            <div class="title-col title-col--normal col">
                                <div class="title-inner-col inner-col">
                                    <h3><?php echo _t('Filter content'); ?></h3>
                                </div>
                            </div>
                        </header>
                        <!-- title END -->
                        <?php if ($_GET['mode'] == 'channel' && is_numeric($_GET['channel'])) { ?>
                            <div class="row">
                                <!-- filter-content -->
                                <div class="filter-content-col col">
                                    <div class="filter-content-inner-col inner-col">
                                        <ul class="simple-list">

                                            <li class="<?php if ($_SESSION['filterContent'] != "videosOnly" && $_SESSION['filterContent'] != "photosOnly") { ?>active<?php } ?>"><a href="<?php echo $basehttp . "/filter/a"; ?>" title="<?php echo _t("Videos & Photos") ?>"><?php echo _t("Videos & Photos") ?></a></li>
                                            <li class="<?php if ($_SESSION['filterContent'] == "videosOnly") { ?>active<?php } ?>"><a href="<?php echo $basehttp . "/filter/videos"; ?>" title="<?php echo _t("Videos Only") ?>"><?php echo _t("Videos Only") ?></a></li>
                                            <li class="<?php if ($_SESSION['filterContent'] == "photosOnly") { ?>active<?php } ?>"><a href="<?php echo $basehttp . "/filter/photos"; ?>" title="<?php echo _t("Photos Only") ?>"><?php echo _t("Photos Only") ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- filter-content END -->
                            </div>
                        <?php } ?>

                        <div class="row">
                            <!-- filter-content -->
                            <div class="filter-content-col col">
                                <div class="filter-content-inner-col inner-col">
                                    <ul class="simple-list">

                                        <li class="<?php if ($_SESSION['filterType'] != "S" && $_SESSION['filterType'] != "G" && $_SESSION['filterType'] != "T") { ?>active<?php } ?>"><a href="<?php echo $basehttp . "/filter-content/a"; ?>" title="<?php echo _t("All") ?>"><?php echo _t("All") ?></a></li>
                                        <li class="<?php if ($_SESSION['filterType'] == "S") { ?>active<?php } ?>"><a href="<?php echo $basehttp . "/filter-content/S"; ?>" title="<?php echo _t("Straight") ?>"><?php echo _t("Straight") ?></a></li>
                                        <li class="<?php if ($_SESSION['filterType'] == "G") { ?>active<?php } ?>"><a href="<?php echo $basehttp . "/filter-content/G"; ?>" title="<?php echo _t("Gay") ?>"><?php echo _t("Gay") ?></a></li>
                                        <li class="<?php if ($_SESSION['filterType'] == "T") { ?>active<?php } ?>"><a href="<?php echo $basehttp . "/filter-content/T"; ?>" title="<?php echo _t("Shemale") ?>"><?php echo _t("Shemale") ?></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- filter-content END -->
                        </div>

                    </div>
                <?php } ?>
                <?php if ((($_GET['controller'] == "index" || (checkNav('channels') && is_numeric($_GET['channel']))) && $_GET['mode'] != "search") && $_GET[mode] != 'photos') { ?>
                    <div class="filter-container">
                        <!-- title -->
                        <header class="row">
                            <div class="title-col title-col--normal col">
                                <div class="title-inner-col inner-col">
                                    <h3><?php echo _t("Duration") ?> <span class="dimmed-desc">(<?php echo _t("minutes") ?>)</span></h3>
                                </div>
                            </div>
                        </header>
                        <!-- title END -->
                        <div class="row">
                            <!-- filter-content -->
                            <div class="filter-content-col col">
                                <div class="filter-content-inner-col inner-col">
                                    <?php
                                    $range_from = (!empty($_GET['durationFrom'])) ? (int) $_GET['durationFrom'] / 60 : 0;
                                    $range_to = (!empty($_GET['durationTo'])) ? (int) $_GET['durationTo'] / 60 : 50;
                                    ?>
                                    <input type="text" data-from="<?php echo $range_from; ?>" data-to="<?php echo $range_to; ?>" data-max="50" data-min="0" data-attr-from="durationFrom" data-attr-to="durationTo" data-multiplication="60" id="range_length_filter" name="filter_length" value="" >
                                </div>
                            </div>
                            <!-- filter-content END -->
                        </div>
                    </div>
                <?php } ?>

                <?php if ($thisController !== 'paysites') { ?>
                    <div class="filter-container">
                        <!-- title -->
                        <header class="row">
                            <div class="title-col title-col--normal col">
                                <div class="title-inner-col inner-col">
                                    <h3>
                                        <?php echo ($thisController == 'channels') ? _t("Alphabetical") : _t("Categories"); ?> <span class="dimmed-desc">(<?php echo getTotalChannels(); ?>)</span>
                                    </h3>
                                </div>
                            </div>
                        </header>
                        <!-- title END -->
                        <div class="row">
                            <!-- filter-content -->
                            <div class="filter-content-col col">
                                <div class="filter-content-inner-col inner-col">
                                    <ul class="simple-list simple-list--channels">
                                        <?php echo showChannels("<li>", "</li>", true); ?>
                                    </ul>
                                </div>
                            </div>
                            <!-- filter-content END -->
                        </div>
                    </div>
                <?php } ?>

                <?php if ($thisController !== 'channels') { ?>
                    <div class="filter-container">
                        <!-- title -->
                        <header class="row">
                            <div class="title-col title-col--normal col">
                                <div class="title-inner-col inner-col">
                                    <h3>
                                        <?php echo ($thisController == 'paysites') ? _t("Alphabetical") : _t("Paysites"); ?>
                                    </h3>
                                </div>
                            </div>
                        </header>
                        <!-- title END -->
                        <div class="row">
                            <!-- filter-content -->
                            <div class="filter-content-col col">
                                <div class="filter-content-inner-col inner-col">
                                    <ul class="simple-list">
                                        <?php echo showPaysites("<li>", "</li>"); ?>
                                    </ul>
                                </div>
                            </div>
                            <!-- filter-content END -->
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <!-- FILTER END -->


    </div>
</aside>
<!-- aside-main END -->
