


<!-- aside-main -->
<aside class="aside-main-col" data-mb="aside" data-opt-filters-on="Show filters" data-opt-filters-off="Hide filters">
    <div class="aside-main-inner-col inner-col">
        <!-- FILTER -->
        <div class="box-container">
            <div class="inner-box-container">
                <div class="filter-container">
                    <!-- title -->
                    <header class="row">
                        <div class="title-col title-col--normal col">
                            <div class="title-inner-col inner-col">
                                <h3><? echo _t("Search Filter"); ?></h3>
                            </div>
                        </div>
                    </header>
                    <form action="<? echo $basehttp ?>/members/" method="post" name="formContact" id="contact-form" class="form-block">
                        <div class="row">
                            <!-- form-item -->
                            <div class="form-item-col col-full col">
                                <div class="form-item-inner-col inner-col">
                                    <label><?php echo _t("Username") ?></label>
                                    <? $ms_name = ($_GET[q] ? $_GET[q] : $_SESSION['ms']['name']) ; ?>
                                    <input type="text" placeholder="<? echo _t("Username"); ?>" value="<? echo $ms_name; ?>" id="name" name="name" class="form-control">
                                </div>
                            </div>
                            <!-- form-item END -->
                        </div>
                        <div class="row">
                            <!-- filter-content -->
                            <div class="form-item-col col-full mrb2 col">
                                <div class="filter-content-inner-col inner-col">
                                    <label><?php echo _t("Age") ?></label>
                                    <?
                                    $age_from = (!empty($_SESSION['ms']['ageFrom'])) ? (int) $_SESSION['ms']['ageFrom'] : 18;
                                    $age_to = (!empty($_SESSION['ms']['ageTo'])) ? (int) $_SESSION['ms']['ageTo'] : 100;
                                    ?>
                                    <input type="text" data-method="onpost" data-from="<? echo $age_from; ?>" data-to="<? echo $age_to; ?>" data-max="98" data-min="18" data-attr-from="ageFrom" data-attr-to="ageTo" data-step="4" id="range_length_filter" name="filter_age" value="" >
                                </div>
                            </div>
                            <!-- filter-content END -->
                        </div>   
                        <div class="row">
                            <!-- filter-content -->
                            <div class="form-item-col col-full col">
                                <div class="form-item-inner-col inner-col">
                                    <label><?php echo _t("Location") ?></label>
                                    <input type="text" placeholder="<?php echo _t("Location"); ?>" value="<? echo $_SESSION['ms']['location']; ?>" id="location" name="location" class="form-control">
                                </div>
                            </div>
                            <!-- filter-content END -->
                        </div>
                        <div class="row">
                            <div class="form-item-col col-full mrb2 col">
                                <div class="form-item-inner-col inner-col">
                                    <label><?php echo _t("Gender") ?></label>
                                    <select name="gender" id="select2" class="select-short" data-style="btn-selectpicker">
                                        <option value="all"><?php echo _t("All") ?></option>
                                        <option <?
                                        if ($_SESSION['ms']['gender'] == 'Male') {
                                            echo 'selected';
                                        }
                                        ?> value='Male'><?php echo _t("Male") ?></option>
                                        <option <?
                                        if ($_SESSION['ms']['gender'] == 'Female') {
                                            echo 'selected';
                                        }
                                        ?> value='Female'><?php echo _t("Female") ?></option>
                                    </select>
                                </div>
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="form-item-col col-full col">
                                <!-- filter-content -->                        
                                <div class="cr-holder checkbox">
                                    <label>
                                        <input class="form-control" type="checkbox" name="hasAvatar" value='1' id="checkbox"  <?
                                        if ($_SESSION[ms][avatar] == 1) {
                                            echo "checked='checked'";
                                        }
                                        ?>>
                                        <span class="sub-label"><?php echo _t("With avatars") ?></span>
                                    </label>
                                </div>
                                <!-- filter-content END -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-item-col col-full col">
                                <!-- filter-content -->                        
                                <div class="cr-holder checkbox">
                                    <label>
                                        <input class="form-control" type="checkbox" name="hasVideos" value='1' id="checkbox"  <?
                                        if ($_SESSION[ms][video] == 1) {
                                            echo "checked='checked'";
                                        }
                                        ?>>
                                        <span class="sub-label"><?php echo _t("Has uploaded videos"); ?></span>
                                    </label>
                                </div>
                                <!-- filter-content END -->
                            </div>
                        </div>                        
                        <div class="row">
                            <div class="form-item-col col-full col">
                                <!-- filter-content -->                        
                                <div class="cr-holder checkbox">
                                    <label>
                                        <input class="form-control" type="checkbox" name="hasPhotos" value='1' id="checkbox"  <?
                                        if ($_SESSION[ms][photo] == 1) {
                                            echo "checked='checked'";
                                        }
                                        ?>>
                                        <span class="sub-label"><?php echo _t("Has uploaded photos"); ?></span>
                                    </label>
                                </div>
                                <!-- filter-content END -->
                            </div>
                        </div>                                                
                        <div class="row">
                            <div class="filter-content-col col">
                                <button name="membersFilter" type="submit" class="btn btn-default"><span class="btn-label"><?php echo _t("Search"); ?></span></button>
                                <? if($_SESSION['ms']) { ?><a href="<? echo $basehttp; ?>/unsetFilters" class="btn btn-primary"><span class="btn-label"><?php echo _t("Reset filters"); ?></span></a><? } ?>
                            </div>
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
        <!-- FILTER END -->


    </div>
</aside>
<!-- aside-main END -->
