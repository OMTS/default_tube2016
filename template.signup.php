<!-- textpage -->
<div class="textpage-col col">
    <div class="textpage-inner-col inner-col">
        <div class="row form-row">
            <!-- form -->
            <div class="form-col col">
                <div class="form-inner-col inner-col">
                    <?php if (isset($_GET['done']) && $_GET['done'] == 'true') { ?>
                        <div class="notification success">
                            <p>
                                <?php if ($require_account_confirmation) { ?>
                                    Verification mail was sent to you email address.<br />
                                    To login please <strong>verify your account</strong> through email verification link.
                                <?php } else { ?>
                                    You may now login at the <a href="<?php echo $basehttp; ?>/login">login page</a>
                                <?php } ?>
                            </p>
                        </div>
                    <?php } ?>	


                    <?php if (!isset($_GET['done']) && $_GET['done'] != true) { ?>
                        <form class="form-block" id="signup-form"  name="signupForm" method="post" action="">
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col col-half">
                                    <div class="form-item-inner-col inner-col">
                                        <input class="form-control" id="signup_username" name="signup_username" type="text" value="<?php echo $thisusername; ?>" placeholder="<?php echo _t("Username") ?>">
                                    </div>
                                </div>
                                <!-- form-item END -->
                                <!-- form-item -->
                                <div class="form-item-col col col-half">
                                    <div class="form-item-inner-col inner-col">
                                        <input class="form-control" id="signup_password" name="signup_password"  type="password" value=""  placeholder="<?php echo _t("Password") ?>">
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col col-full">
                                    <div class="form-item-inner-col inner-col">
                                        <input class="form-control" id="signup_email" name="signup_email" type="text"  value="<?php echo $thisemail; ?>" placeholder="<?php echo _t("Email") ?>">
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>

                            <?php if ($enable_signup_captcha) { ?>
                                <div class="row">
                                    <!-- form-item -->
                                    <div class="form-item-col col col-half">
                                        <div class="form-item-inner-col inner-col">
                                            <div class="captcha-wrapper">
                                                <img src="<?php echo $basehttp; ?>/captcha.php?<?php echo time(); ?>" class="captcha captcha-img">
                                                <input class="form-control captcha-input" id="signup_captchaaa" name="captchaaa" type="text" value="" placeholder="<?php echo _t("Human?") ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- form-item END -->
                                </div>
                            <?php } ?>
							<div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col form-item--actions">
                                    <div class="form-item-inner-col inner-col">
                                       <input type="checkbox" name="signup_tos" class="fr__input -input-text" /> <? echo _t("Click here to indicate that you have read and agree to the"); ?> <a href='<? echo $basehttp; ?>/static/tos.html'><? echo _t("terms of service and privacy policy"); ?></a>
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
							
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col form-item--actions">
                                    <div class="form-item-inner-col inner-col">
                                        <button class="btn btn-default" type="submit" name="Submit"><span class="btn-label"><?php echo _t("Register") ?></span></button>
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
                            <?php if (!$_SESSION['userid'] && ($enable_facebook_login || $enable_twitter_login)) { ?>
                                <div class="row">
                                    <?php if (!$_SESSION['userid'] && $enable_facebook_login) { ?>
                                        <!-- form-item -->
                                        <div class="form-item-col col form-item--social form-item--social-fb">
                                            <div class="form-item-inner-col inner-col">
                                                <a class="btn btn-social-login btn-social-fb" href="<?php echo $basehttp; ?>/includes/facebook/facebook.php"><img src="<?php echo $basehttp; ?>/core/images/facebook-login-button.png" alt="<?php echo _t("Login by Facebook") ?>" /></a>
                                            </div>
                                        </div>
                                        <!-- form-item END -->
                                    <?php } ?>
                                    <?php if (!$_SESSION['userid'] && $enable_twitter_login) { ?>
                                        <!-- form-item -->
                                        <div class="form-item-col col form-item--social form-item--social-tw">
                                            <div class="form-item-inner-col inner-col">
                                                <a class="btn btn-social-login btn-social-tw" href="<?php echo $twitter->getAuthenticateUrl(); ?>&force_login=true"><img src="<?php echo $basehttp; ?>/core/images/twitter-login-button.png" alt="<?php echo _t("Login by Twitter") ?>" /></a>
                                            </div>
                                        </div>
                                        <!-- form-item END -->
                                    <?php } ?>
                                </div>
                            <?php } ?>

                        </form>
                    <?php } ?>
                </div>
            </div>
            <!-- form END -->
        </div>
    </div>
</div>
<!-- textpage END -->