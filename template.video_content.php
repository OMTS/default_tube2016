            <!-- VIDEO CONTENT -->
            <!-- stage -->

            <div class="stage-col col">
                <div class="stage-inner-col inner-col">
					<? 
					//check if videoRatio should be used. It should be false only when in VOD Mode user does not have access to video or in Paysite Mode where user is not premium and mode is set to thumbnails (mode 2)
					$showVideoRatio = false;
					if(!$vodMode && $paysiteMode != 2) { 
						$showVideoRatio = true; 
					} else { 
						if($_SESSION['premium']) { 
							$showVideoRatio = true; 
						}
						if($vodMode && billingCheckUserContentAuth($_SESSION['userid'],$rrow['record_num'])) { 
							$showVideoRatio = true; 
						}
					}
					
					?>
					
					
                    <div class="inner-stage" <?php if($showVideoRatio) { echo videoRatio($rrow); }  ?>>
						<? if (($vodMode && !billingCheckUserContentAuth($_SESSION['userid'], $rrow['record_num'])) || (!$_SESSION['premium'] && $paysiteMode == '2' && !billingCheckUserContentAuth($_SESSION['userid'], $rrow['record_num']))) { ?>
							<center><span style='font-size: 18px; font-weight: bold;'><? echo _t("You must purchase access to this video in order to view it."); ?></span></center><br>
							<div class="preview-thumbs">
								<?
								$dirname = str_replace('.flv', '', $rrow['orig_filename']);
								$subdir = $rrow['filename'][0] . '/' . $rrow['filename'][1] . '/' . $rrow['filename'][2] . '/' . $rrow['filename'][3] . '/' . $rrow['filename'][4] . '/';
								$dirname = $subdir . $dirname;
								?>
								<? for($i=1;$i<=8;$i++){ ?>
								
									<img src="<? echo $thumb_url; ?>/<? echo $dirname; ?>/<? echo $rrow['orig_filename']; ?>-<? echo $i; ?>.jpg" title="<? echo $row['title'].'-'.$i ?>" style='width: 24%'>
								
								<? } ?>
								<div class="signup-link" style='text-align: center;'>
									<br>
									<? 
									if($vodMode) { 
										$purchaseLink = "$basehttp/purchase/$rrow[record_num]"; 
									} else { 
										$purchaseLink = "$basehttp/signup"; 
									}
									?>
									<a class='btn btn-default btn-sm' href="<? echo $purchaseLink; ?>"><? echo _t("Click Here To Purchase Access!"); ?></a><br><br>
								</div>
							</div>
						<? } else { ?>
							
								<?php displayPlayerWrapper($rrow, "100%", "100%"); ?>
							
						<? } ?>
                    </div>
                </div>
            </div>
            <!-- stage END -->
            <?php if(((detectMobile() && $sceneSelectionMobile) || (!detectMobile() && $sceneSelectionDesktop)) && !$rrow['embed'] && !$rrow['vr']) { ?>
                <div class="item-tr-col col">
                    <div class="item-tr-inner-col inner-col vtt-thumbs-col">
                        <div class="row vtt-thumbs">
                            <? echo displaySceneSelection(); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <!-- item-tr -->
            <div class="item-tr-col col">
                <div class="item-tr-inner-col inner-col">
                    <h1><?php echo $rrow['title']; ?></h1>
                    <!-- rating -->
                    <div class="rating-col col">
                        <div class="rating-inner-col inner-col">
                            <?php include($basepath . '/includes/rating/templateTh.php'); ?>
                        </div>
                    </div>
                    <!-- rating END -->
                </div>
            </div>
            <!-- item-tr END -->

            <!-- item-tabs -->
            <div class="item-tabs-col col">
                <div class="item-tabs-inner-col inner-col">
                    <ul class="tabs-list">
                        <li class="active"><a href="#" title="<?php echo _t("About") ?>" data-mb="tab" data-opt-tab="about"><span class="icon i-info"></span><span class="sub-label"><?php echo _t("About") ?></span></a></li>
                        <li><a data-mb="tab" href="#" title="<?php echo _t("Share") ?>" data-opt-tab="share"><span class="icon i-share"></span><span class="sub-label"><?php echo _t("Share") ?></span></a></li>
                        <?php if (empty($rrow['embed'])) { ?>
                            <li><a href="<?php echo $video_url . '/' . $rrow['filename'][0] . '/' . $rrow['filename'][1] . '/' . $rrow['filename'][2] . '/' . $rrow['filename'][3] . '/' . $rrow['filename'][4] . '/' . $rrow['filename']; ?>" title="<?php $row['title']; ?>" data-mb="download" download="<?php echo $rrow["filename"]; ?>"><span class="icon i-download"></span><span class="sub-label"><?php echo _t('Download'); ?></span></a></li>
                        <?php } ?>
                        <?php if ($allowAddFavorites) { ?>
                            <li><a href="<?php echo $basehttp; ?>/action.php?action=add_favorites&id=<?php echo $rrow['record_num']; ?>" data-mb="modal" data-opt-type="ajax" data-opt-close="<?php echo _t("Close") ?>" data-toggle="tooltip" title="<?php echo _t("Add to favorites") ?>"><span class="icon i-heart"></span></a></li>
                        <?php } ?>
                        <li><a href="<?php echo $basehttp; ?>/action.php?action=reportVideo&id=<?php echo $rrow['record_num']; ?>" data-mb="modal" data-opt-type="iframe" data-opt-iframe-width="100%"  data-opt-iframe-height="400px" data-toggle="tooltip" title="<?php echo _t("Report content") ?>"><span class="icon i-flag"></span></a></li>
                    </ul>

                </div>
            </div>
            <!-- item-tabs END -->

            <!-- tabs -->
            <div class="tabs-col col">
                <div class="tabs-inner-col inner-col">

                    <!-- TAB -->
                    <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="about">
                        <div class="row">
                            <!-- tab-block -->
                            <div class="tab-block-col col">
                                <div class="tab-block-inner-col inner-col">

                                    <div class="d-container">
                                        <div class="submitter-container">
                                            <span class="inner-wrapper">
                                                <?php if (!$rrow['submitter'] == 0) { ?><a href="<?php echo generateUrl('user', $rrow['username'], $rrow['submitter']); ?>" title="<?php echo $rrow['username']; ?>"><?php } ?>
                                                    <span class="avatar">
                                                        <span class="avatar-img">
                                                            <?php echo getUserAvatar($rrow['submitter']); ?>
                                                        </span>
                                                    </span>
                                                    <span class="user-name">
                                                        <span class="sub-label"><?php echo _t("Submitted by") ?></span>
                                                        <?php if ($rrow['submitter'] == 0) { ?><?php echo _t("Anonymous") ?><?php } else { ?><?php echo $rrow['username']; ?><?php } ?>
                                                    </span>
                                                    <?php if (!$rrow['submitter'] == 0) { ?></a><?php } ?>
                                            </span>
                                        </div>

                                        <div class="stats-container">
                                            <ul class="stats-list">
                                                <li><span class="icon i-clock"></span><span class="sub-label"><?php echo sec2time($rrow['length']); ?></span></li>
                                                <li><span class="icon i-eye"></span><span class="sub-label"><?php echo $rrow['views']; ?></span></li>
                                                <li><span class="icon i-calendar"></span><span class="sub-label"><?php echo $rrow['date_added']; ?></span></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <?php $_keywords = buildTags($rrow['keywords']); ?>
                                    <?php $_channels = buildChannels($rrow['record_num']); ?>
                                    <?php $_description = $rrow['description']; ?>
                                    <?php $_pornstar = showPornstar($rrow['record_num'], "<li>", "</li>"); ?>

                                    <?php if (!empty($_keywords) || !empty($_channels) || !empty($_description) || $_pornstar != false) { ?>
                                        <div class="expand-block-more" data-opt-expand-target="more-info">
                                            <?php if (!empty($_description)) { ?>
                                                <div class="d-container">
                                                    <div class="main-description">
                                                        <?php echo $_description; ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if ($_pornstar != false) { ?>
                                                <div class="d-container">
                                                    <div class="tags-block">
                                                        <span class="icon i-star"></span>
                                                        <span class="sub-label"><?php echo _t("Models") ?>:</span>
                                                        <ul class="models-list"><?php echo $_pornstar; ?></ul>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if (!empty($_keywords)) { ?>
                                                <div class="d-container">
                                                    <div class="tags-block">
                                                        <span class="icon i-tag"></span>
                                                        <span class="sub-label"><?php echo _t("Tags") ?>:</span>
                                                        <?php echo $_keywords; ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if (!empty($_channels)) { ?>
                                                <div class="d-container">
                                                    <div class="tags-block">
                                                        <span class="icon i-folder"></span>
                                                        <span class="sub-label"><?php echo _t("Categories") ?>:</span>
                                                        <?php echo $_channels; ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>

                                    <?php if (!empty($_keywords) || !empty($_channels) || !empty($_description)) { ?>
                                        <!-- expand-trigger -->
                                        <div class="expand-trigger-col col">
                                            <div class="expand-trigger-inner-col inner-col">
                                                <a href="#" title="" class="btn btn-header" data-mb="expand" data-opt-target="more-info"><span class="sub-label-off"><?php echo _t("Show more") ?></span><span class="sub-label-on"><?php echo _t("Show less") ?></span></a>
                                            </div>
                                        </div>
                                        <!-- expand-trigger END -->
                                    <?php } ?>

                                </div>
                            </div>
                            <!-- tab-block END -->
                        </div>
                    </div>
                    <!-- TAB END -->
                    <!-- TAB -->
                    <div class="tab-wrapper" data-mb="tab-content" data-opt-tab-content="share">
                        <div class="row">
                            <!-- tab-block -->
                            <div class="tab-block-col col">
                                <div class="tab-block-inner-col inner-col">

                                    <div class="d-container">
                                        <div class="text-share">
                                            <textarea readonly>&lt;iframe src='<?php echo $basehttp; ?>/embed/<?php echo $rrow['record_num']; ?>' frameborder='0' height='400' width='600'&gt;&lt;/iframe&gt;&lt;br&gt;&lt;strong&gt;<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>&lt;/strong&gt; - powered by &lt;a href='<?php echo $basehttp; ?>'&gt;<?php echo $sitename; ?>&lt;/a&gt;</textarea>
                                        </div>
                                    </div>
                                    <div class="d-container">
                                        <div class="social-media-share">
                                            <ul class="share-list">
                                                <li><a data-mb="popup" data-opt-width="500" data-opt-height="400" href="//www.facebook.com/share.php?u=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>&title=<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" class="social social-fb"><span class="icon i-fb"></span></a></li>
                                                <li><a data-mb="popup" data-opt-width="500" data-opt-height="400" href="//twitter.com/home?status=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>+<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" class="social social-tw"><span class="icon i-tw"></span></a></li>
                                                <li><a data-mb="popup" data-opt-width="500" data-opt-height="400" href="//www.tumblr.com/share?v=3&u=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>&t=<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>"><span class="icon i-tr"></span></a></li>
                                                <li><a data-mb="popup" data-opt-width="500" data-opt-height="400" href="https://plus.google.com/share?url=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" class="social social-gp"><span class="icon i-gp"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- tab-block END -->
                        </div>
                    </div>
                    <!-- TAB END -->


                </div>
            </div>
            <!-- tabs END -->
        </div>
    </div>
</div>

<?php if ($allowDisplayComments) { ?>
    <div class="box-container">
        <div class="inner-box-container">
            <header class="row">
                <div class="title-col title-col--normal col">
                    <div class="title-inner-col inner-col">
                        <h1><?php echo _t("Comments") ?> <span class="dimmed-desc">(<?php echo getTotalComments($rrow['record_num']); ?>)</span></h1>
                    </div>
                </div>
            </header>
            <div class="row">
                <!-- comments -->
                <section class="comments-col col" id="comments">
                    <div class="comments-inner-col inner-col">
                        <?php $contentID = $rrow['record_num']; ?>
                        <?php $commentsType = 0; ?>
                        <?php include('widgets/widget.comments.php'); ?>
                    </div>
                </section>
                <!-- comments END -->
            </div>
        </div>
    </div>
<?php } ?>