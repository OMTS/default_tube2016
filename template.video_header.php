<?
$colorClass = ($_COOKIE["mbColorScheme"] == 1) ? " inverted-colors" : "";
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7<? echo $colorClass ?>"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie-7-only<? echo $colorClass ?>"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie-8-only<? echo $colorClass ?>"><![endif]-->
<!--[if gte IE 9]><!--> <html class="no-js no-filter<? echo $colorClass ?>"><!--<![endif]-->
    <head>
        <? getWidget('widget.header_scripts.php'); ?>
    </head>
    <body class="page-<? echo bodyClasses(); ?>">
        <? getWidget('widget.network.php'); ?>


        <section class="page-wrap">
            <? getTemplate('template.nav.php'); ?>
            <? getWidget('widget.ad_top.php'); ?>




            <section class="content-sec">
                <div class="wrapper">
                    <? getTemplate('template.notifications.php'); ?>
                    <div class="row">

                        <!-- content -->
                        <div class="content-col col">
                            <div class="content-inner-col inner-col">




                                <!-- main -->
                                <main class="main-col item-page">
                                    <div class="main-inner-col inner-col">

                                        <div class="box-container">
                                            <div class="inner-box-container">
                                                <!-- title END -->
                                                <div class="row">