<div class="row">
    <!-- ajax-comments -->
    <div class="ajax-comments-col col col-full">
        <div class="ajax-comments-inner-col inner-col" data-mb="load-comments" data-opt-url="<?php echo $template_url; ?>/template.ajax_comments.php" data-opt-id="<?php echo $contentID; ?>" data-opt-type="<?php echo $commentsType; ?>">

        </div>
    </div>
    <!-- ajax-comments END -->
</div>

<div class="row">
    <!-- expand-trigger -->
    <div class="expand-trigger-col expand-trigger--no-offset col">
        <div class="expand-trigger-inner-col inner-col">
            <a href="#" title="" class="btn btn-header" data-mb="expand" data-opt-limit="5" data-opt-target="comments"><span class="sub-label-off"><?php echo _t("Show more") ?></span><span class="sub-label-on"><?php echo _t("Show less") ?></span></a>
        </div>
    </div>
    <!-- expand-trigger END -->
</div>

<?php if ($allowAddComments) { ?>
    <?php if ($_SESSION['userid']) { ?>
        <div class="row">
            <!-- comment-alert -->
            <div class="comment-alert-col col-full col">
                <div class="comment-alert-inner-col inner-col" data-mb="comment-alert">
                </div>
            </div>
            <!-- comment-alert END -->
        </div>
        <!-- title -->
        <div class="row">
            <div class="title-col title-col--comment col">
                <div class="title-inner-col inner-col">
                    <h3><?php echo _t("Add Comment") ?>:</h3>
                </div>
            </div>
        </div>
        <!-- title END -->

        <div class="row">
            <!-- comment-form -->
            <div class="comment-form-col col col-full">
                <div class="comment-form-inner-col inner-col">
                    <div id="comment-form">
                        <div class="c-thumb">
                            <?php echo getUserAvatar($_SESSION[userid]); ?>
                        </div>
                        <form id="myform" name="comments" class="sendCommentsBox" action="" data-mb="add-comment">
                            <input type='hidden' name='id' id='id' value='<?php echo $contentID; ?>' />
                            <input type='hidden' name='type' id='type' value='<?php echo $commentsType; ?>' />
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col-full col">
                                    <div class="form-item-inner-col inner-col">
                                        <textarea placeholder="<?php echo _t("Your comment") ?>" id="comment" name="comment"></textarea>
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col form-item--captcha">
                                    <div class="form-item-inner-col inner-col">
                                        <div class="captcha-wrapper">
                                            <script type="text/javascript"></script>
                                            <img class="captcha captcha-img" src="<?php echo $basehttp; ?>/captcha.php?<?php echo rand(0, 9999); ?>">
                                            <input type="text" placeholder="<?php echo _t("Human?") ?>" value="" name="captcha" id="captchaCom" class="form-control captcha-input">
                                        </div>
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
                            <div class="row">
                                <!-- form-item -->
                                <div class="form-item-col col form-item--actions">
                                    <div class="form-item-inner-col inner-col">
                                        <button id="button" name="button" type="submit" class="btn btn-default"><span class="btn-label"><?php echo _t("Post comment") ?></span></button>
                                    </div>
                                </div>
                                <!-- form-item END -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- comment-form END -->
        </div>
    <?php } else { ?>
        <div class="notification alert-warning">
            <p><?php echo _t("You must be logged in to post wall comments") ?>. <?php echo _t("Please") ?> <a href='<?php echo $basehttp; ?>/login'><?php echo _t("Login") ?></a> <?php echo _t("or") ?> <a href='<?php echo $basehttp; ?>/signup'><?php echo _t("Signup (free)") ?></a>.</p>
        </div>
    <?php } ?>
<?php } ?>