<footer>
    <div class="wrapper">
        <div class="row">
            <!-- footer-top -->
            <div class="footer-top-col col col-full">
                <div class="footer-top-inner-col inner-col">
                    <ul class="inline-list nav-footer-list">
                        <li class="label-el"><span class="label"><?php echo _t("Movies") ?></span></li>
                        <li><a title="<?php echo _t("New Releases") ?>" href="<? echo $basehttp; ?>/most-recent/"><?php echo _t("New Releases") ?></a></li>
                        <li><a title="<?php echo _t("Most Viewed") ?>" href="<? echo $basehttp; ?>/most-viewed/"><?php echo _t("Most Viewed") ?></a></li>
                        <li><a title="<?php echo _t("Top Rated") ?>" href="<? echo $basehttp; ?>/top-rated/"><?php echo _t("Top Rated") ?></a></li>
                        <li><a title="<?php echo _t("Most Discussed") ?>" href="<? echo $basehttp; ?>/most-discussed/"><?php echo _t("Most Discussed") ?></a></li>
                    </ul>
                    <ul class="inline-list nav-footer-list">
                        <li class="label-el"><span class="label"><?php echo _t("Account") ?></span></li>
                        <? if (!isLoggedIn()) { ?>
                            <li><a title="<?php echo _t("Login") ?>" href="<? echo $basehttp; ?>/login"><?php echo _t("Login") ?></a></li>
                            <li><a title="<?php echo _t("Create Free Account") ?>" href="<? echo $basehttp; ?>/signup"><?php echo _t("Create Free Account") ?></a></li>
                        <? } else { ?>
                            <li><a title="<?php echo _t("My profile") ?>" href="<? echo $basehttp; ?>/my-profile"><?php echo _t("My profile") ?></a></li>
                            <li><a title="<?php echo _t("Edit profile") ?>" href="<? echo $basehttp; ?>/edit-profile"><?php echo _t("Edit profile") ?></a></li>
                            <li><a title="<?php echo _t("Logout") ?>" href="<? echo $basehttp; ?>/logout"><?php echo _t("Logout") ?></a></li>
                        <? } ?>
                        <li><a title="<?php echo _t("Contact") ?>" href="<? echo $basehttp; ?>/contact"><?php echo _t("Contact") ?></a></li>
						<li><a class='i-rss' title="<?php echo _t("RSS") ?>" href="<? echo $basehttp; ?>/rss"><?php echo _t("RSS") ?></a></li>
                    </ul>
                </div>
            </div>
            <!-- footer-top END -->
        </div>
        <div class="row">
            <!-- copyright -->
            <div class="copyright-col col col-full">
                <div class="copyright-inner-col inner-col">
                    <span class="copyright-text inline-text"><a href='https://mechbunny.com/' target='_blank'>Mechbunny Tube Script</a> &copy; Copyright <? echo date('Y'); ?> </span>
                    <ul class="inline-list nav-footer-list">
                        <li><a title="<?php echo _t("DMCA Notice") ?>" href="<? echo $basehttp; ?>/static/dmca.html"><?php echo _t("DMCA Notice") ?></a></li>
                        <li><a title="<?php echo _t("Terms of Use") ?>" href="<? echo $basehttp; ?>/static/tos.html"><?php echo _t("Terms of Use") ?></a></li>
                        <li><a title="<?php echo _t("18 U.S.C. 2257 Record-Keeping Requirements Compliance Statement") ?>" href="<? echo $basehttp; ?>/static/2257.html"><?php echo _t("18 U.S.C. 2257 Record-Keeping Requirements Compliance Statement") ?></a></li>
                    </ul>
                </div>
            </div>
            <!-- copyright END -->
        </div>
    </div>
	<?php getWidget('widget.footer_custom.php'); ?>
</footer>