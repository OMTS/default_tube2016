<?
/*
by default we disable this for mobile because the player is usually too small for a 300x250. If you want to make it show on mobiles too, remove this "if" and the closing "}" at the end
*/
?>
<? if(!detectMobile()) {  ?>
	<? if(!$_GET['t']) { ?>

		<div id="playerOverlay">
			<a href='https://www.mechbunny.com/tube-script' target='_blank'><img src='https://www.mechbunny.com/images/sqr-3.png'></a>
			<a id='playerClose' onClick="$(this).parent().hide(); videojsPlayer.play(); return false; " href='#'>Close &amp; Play</a>
		</div>
		<style>
		#playerOverlay {
			background:rgba(0,0,0,0.5);
			width: 320px;
			height: 300px;
			position:absolute;
			text-align: center;
			top:50%;
			left: 50%;
			padding-top: 10px;
			margin-left: -160px;
			margin-top: -150px;
			z-index:3;
		}
		#playerClose { 
			display: inline-block;
			margin-top: 10px;
			font-weight: bold;
		}
		</style>

		<!-- The below code makes the ad also show on paused state. Can be removed, and should be removed if using separate mobile ad network code. -->
		<script>
			videojsPlayer.on("pause", function () {
				if(videojsPlayer.hasClass('vjs-scrubbing') == false) {
					$('#playerOverlay').fadeIn(500); 
				}
				
			});
			videojsPlayer.on("play", function () {
				$('#playerOverlay').hide();
			});
			
			videojsPlayer.on("seeking", function () {
				$('#playerOverlay').hide();
			});
			
    
		</script>
		<!-- EOF -->

	<? } ?>
<? } ?>